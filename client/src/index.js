import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import createSagaMiddleware from 'redux-saga';

import boardReducer from "./store/reducers/Board";
import cardReducer from "./store/reducers/Card";
import authReducer from "./store/reducers/Auth";
import boardSelectorReducer from "./store/reducers/BoardSelector";
import { watchBoard, watchCard, watchAuth, watchBoardSelector } from './store/sagas/index';
import 'bootstrap/dist/css/bootstrap.min.css';

// const composeEnhancers = (process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null) || compose;
const composeEnhancers = process.env.NODE_ENV === 'development' ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const rootReducer = combineReducers({
  board: boardReducer,
  card: cardReducer,
  auth: authReducer,
  boardSelector: boardSelectorReducer,
});

const sagaMiddleware = createSagaMiddleware();

const store = createStore(rootReducer, composeEnhancers(
  applyMiddleware(sagaMiddleware)
));

sagaMiddleware.run(watchBoard);
sagaMiddleware.run(watchCard);
sagaMiddleware.run(watchAuth);
sagaMiddleware.run(watchBoardSelector);

const app = (
  <Provider store={store}>
    <BrowserRouter>
      <React.StrictMode>
        <App />
      </React.StrictMode>
    </BrowserRouter>
  </Provider>
);

ReactDOM.render(app, document.getElementById("root"));

serviceWorker.unregister();
