import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "./store/actions";
import Board from "./containers/Board/Board";
import Login from "./containers/Login/Login";
import Signup from "./containers/Signup/Signup";
import BoardSelector from "./containers/BoardSelector/BoardSelector";
import ProtectedRoute from "./hoc/ProtectedRoute/ProtectedRoute";

class App extends Component {
  componentDidMount() {
    this.props.autoSignUp();
  }

  render() {
    return (
      <Switch>
        {/* <Route path="/card/:cardId">
          <Board modal={true} />
        </Route> */}
        {/* <Route path="/board/:boardId">
          <Board modal={false} />
        </Route> */}
        <ProtectedRoute component={Board} path="/card/:cardId" modal={true} />
        <ProtectedRoute component={Board} path="/board/:boardId" modal={false} />
        {/* <ProtectedRoute component={BoardSelector} path="/:userId/boards" /> */}
        <ProtectedRoute component={BoardSelector} path="/:userId/boards" />
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/signup">
          <Signup />
        </Route>

        {/* <Route path="/:userId/boards">
          <BoardSelector />
        </Route> */}
        {/* this is temporary for easy of development */}
        <Route
          path="/"
          render={() => {
            return this.props.isAuth ? 
            <BoardSelector />
            // <Board modal={false} />
            : 
            <Redirect to="/login" />;
          }}
        />
      </Switch>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    autoSignUp: () => dispatch(actions.checkAuth()),
  };
};

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.token ? true : false,
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
