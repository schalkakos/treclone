import * as actionTypes from "../actions/actionTypes";
import update from "immutability-helper";

const initialState = {
  cardData: {
    checklistItems: [],
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_CARD: {
      return update(state, { isFetching: { $set: true } });
    }
    case actionTypes.SET_CARD: {
      return update(state, {
        isFetching: { $set: false },
        cardData: { $set: action.response },
      });
    }
    case actionTypes.UPDATE_CHECKLIST_NAME_SUCCESS: {
      return update(state, {
        cardData: {
          checklist: { $set: true },
          checklistName: { $set: action.name },
        },
      });
    }
    case actionTypes.CREATE_CHECKLIST_ITEM_SUCCESS: {
      return update(state, {
        cardData: {
          checklistItems: {
            $push: [action.response],
          },
        },
      });
    }
    case actionTypes.UPDATE_CHECKLIST_ITEM_NAME_SUCCESS: {
      const checklistItemIndex = state.cardData.checklistItems.findIndex(
        (obj) => {
          return obj._id === action.checklistItemId;
        }
      );
      // console.log(state.cardData.checklistItems[checklistIndex].name)

      return update(state, {
        cardData: {
          checklistItems: {
            [checklistItemIndex]: {
              name: { $set: action.checklistItemName },
            },
          },
        },
      });
    }
    case actionTypes.UPDATE_TASK_COMPLETION_SUCCESS: {
      const checklistItemIndex = state.cardData.checklistItems.findIndex(
        (obj) => {
          return obj._id === action.checklistItemId;
        }
      );

      return update(state, {
        cardData: {
          checklistItems: {
            [checklistItemIndex]: {
              done: { $set: action.completionState },
            },
          },
        },
      });
    }
    case actionTypes.DELETE_CHECKLIST_ITEM_SUCCESS: {
      const checklistItemIndex = state.cardData.checklistItems.findIndex(
        (obj) => {
          return obj._id === action.checklistItemId;
        }
      );

      return update(state, {
        cardData: {
          checklistItems: {
            $splice: [[checklistItemIndex, 1]],
          },
        },
      });
    }
    case actionTypes.DELETE_CHECKLIST: {
      return update(state, {
        cardData: {
          checklist: { $set: false },
          checklistName: { $set: "" },
          checklistItems: { $set: [] },
        },
      });
    }
    case actionTypes.UPDATE_DESCRIPTION_SUCCESS: {
      return update(state, {
        cardData: {
          description: {
            $set: action.updatedDescription,
          },
        },
      });
    }
    case actionTypes.UPDATE_CARD_TITLE: {
      return update(state, {
        cardData: {
          title: { $set: action.updatedTitle },
        },
      });
    }
    case actionTypes.DELETE_CARD_SUCCESS: {
      return update(state, { $set: initialState });
    }
    case actionTypes.UPDATE_DUE_DATE_SUCCESS: {
      if (action.dueDate === null) {
        return update(state, {
          cardData: {
            dueDate: { $set: action.dueDate },
            dueDateDone: { $set: false },
          },
        });
      } else {
        return update(state, {
          cardData: {
            dueDate: { $set: action.dueDate },
          },
        });
      }
    }
    case actionTypes.UPDATE_DUE_DATE_DONE_SUCCESS: {
      const { dueDateDone } = action;
      return update(state, {
        cardData: {
          dueDateDone: { $set: dueDateDone },
        },
      });
    }
    case actionTypes.MOVE_CARD_SUCCESS: {
      const { listId, newCardPos } = action;
      console.log(listId, newCardPos)
      console.log(state)
      return update(state, {
        cardData: {
          listId: {$set: listId},
          order: {$set: newCardPos + 1}
        }
      });
      // return update
    }
    default:
      return state;
  }
};

export default reducer;
