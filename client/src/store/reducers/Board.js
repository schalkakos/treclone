import * as actionTypes from "../actions/actionTypes";
import update from "immutability-helper";
import { getListIndex, getCardIndex, updateOldListOrder } from "../utils";

const initialState = {
  error: null,
  boardData: {
    boardContent: [],
  },
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CREATE_LIST_SUCCESS: {
      return update(state, {
        boardData: {
          boardContent: {
            $push: [action.response],
          },
        },
      });
    }
    case actionTypes.UPDATE_LIST_TITLE_SUCCESS: {
      const index = getListIndex(state.boardData.boardContent, action.listId);

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              title: { $set: action.title },
            },
          },
        },
      });
    }
    case actionTypes.UPDATE_LIST_ORDER_SUCCESS: {
      const index = getListIndex(state.boardData.boardContent, action.listId);

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              order: { $set: action.order },
            },
          },
        },
      });
    }
    case actionTypes.UPDATE_LIST_SORT_SUCCESS: {
      console.log(action.listId)
      const index = getListIndex(state.boardData.boardContent, action.listId);

      console.log(index)

      const sortByKey = Object.keys(action.sortBy);
      // console.log(state.boardData.boardContent[listIndex].cards);

      const cards = state.boardData.boardContent[index.listIndex].cards.sort(
        (a, b) => {
          if (action.sortBy[sortByKey] === 1) {
            return a[sortByKey] > b[sortByKey] ? 1 : -1;
          } else {
            return a[sortByKey] < b[sortByKey] ? 1 : -1;
          }
        }
      );
      // console.log(cards);

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              sortBy: { $set: action.sortBy },
              cards: {
                $set: cards,
              },
            },
          },
        },
      });
    }
    case actionTypes.ADD_CARD_SUCCESS: {
      const index = getListIndex(
        state.boardData.boardContent,
        action.response.listId
      );
      // console.log("listIndex", listI)

      // console.log(state.boardContent[listI])

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              cards: {
                $push: [action.response],
              },
            },
          },
        },
      });
    }
    case actionTypes.SET_BOARD: {
      // console.log(action.response);
      return update(state, {
        error: {$set: false},
        boardData: { $set: action.response },
      });
    }
    case actionTypes.GET_BOARD_FAIL: {
      return update(state, {
        error: {$set: true}
      })
    }
    case actionTypes.DELETE_LIST_SUCCESS: {
      const index = getListIndex(state.boardData.boardContent, action.listId);
      // console.log("listIndex", listI)

      // console.log(state.boardContent[listI])

      return update(state, {
        boardData: {
          boardContent: { $splice: [[index.listIndex, 1]] },
        },
      });
    }
    case actionTypes.UPDATE_CARD_TITLE_SUCCESS: {
      const index = getCardIndex(
        state.boardData.boardContent,
        action.listId,
        action.cardId
      );

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              cards: {
                [index.cardIndex]: {
                  title: { $set: action.updatedTitle },
                },
              },
            },
          },
        },
      });
    }
    case actionTypes.DELETE_CARD_SUCCESS: {
      const index = getCardIndex(
        state.boardData.boardContent,
        action.listId,
        action.cardId
      );

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              cards: {
                $splice: [[index.cardIndex, 1]],
              },
            },
          },
        },
      });
    }
    case actionTypes.UPDATE_DUE_DATE_SUCCESS: {
      console.log(state.boardData.boardContent,
        action.listId,
        action.cardId)
      const index = getCardIndex(
        state.boardData.boardContent,
        action.listId,
        action.cardId
      );

      if (action.dueDate === null) {
        return update(state, {
          boardData: {
            boardContent: {
              [index.listIndex]: {
                cards: {
                  [index.cardIndex]: {
                    dueDate: { $set: action.dueDate },
                  },
                },
              },
            },
          },
        });
      } else {
        return update(state, {
          boardData: {
            boardContent: {
              [index.listIndex]: {
                cards: {
                  [index.cardIndex]: {
                    dueDate: { $set: action.dueDate },
                    dueDateDone: { $set: false },
                  },
                },
              },
            },
          },
        });
      }
    }
    case actionTypes.UPDATE_DUE_DATE_DONE_SUCCESS: {
      const { cardId, listId, dueDateDone } = action;

      const index = getCardIndex(state.boardData.boardContent, listId, cardId);

      return update(state, {
        boardData: {
          boardContent: {
            [index.listIndex]: {
              cards: {
                [index.cardIndex]: {
                  dueDateDone: {
                    $set: dueDateDone,
                  },
                },
              },
            },
          },
        },
      });
    }
    //temporary solution
    case actionTypes.MOVE_CARD_SUCCESS: {
      console.log("move Card ran")
      const { oldListPos, oldCardPos, newListPos, newCardPos } = action;

      const movedCard = {
        ...state.boardData.boardContent[oldListPos].cards[oldCardPos],
      };

      movedCard.order = newCardPos + 1;

      if (oldListPos === newListPos) {
        let updatedState = updateOldListOrder(state, oldListPos, oldCardPos);

        return update(updatedState, {
          boardData: {
            boardContent: {
              [oldListPos]: {
                cards: {
                  $splice: [[newCardPos, 0, movedCard]],
                },
              },
            },
          },
        });
      } else {
        const newListId = state.boardData.boardContent[newListPos]._id;
        movedCard.listId = newListId;

        let updatedState = updateOldListOrder(state, oldListPos, oldCardPos);

        const newCardsLength =
          updatedState.boardData.boardContent[newListPos].cards.length;
        for (let i = newCardPos; i < newCardsLength; i++) {
          updatedState = update(updatedState, {
            boardData: {
              boardContent: {
                [newListPos]: {
                  cards: {
                    [i]: {
                      order: { $set: i + 2 },
                    },
                  },
                },
              },
            },
          });
        }
        return update(updatedState, {
          boardData: {
            boardContent: {
              [newListPos]: {
                cards: {
                  $splice: [[newCardPos, 0, movedCard]],
                },
              },
            },
          },
        });
        // console.log(
        //   "old",
        //   updatedState.boardData.boardContent[oldListPos].cards
        // );
        // console.log(
        //   "new",
        //   updatedState.boardData.boardContent[newListPos].cards
        // );
        // console.log(movedCard);
        // return updatedState;
      }
    }
    case actionTypes.MOVE_LIST_SUCCESS: {
      const { oldListPos, newListPos } = action;

      const movedList = { ...state.boardData.boardContent[oldListPos] };
      movedList.order = newListPos + 1;

      let updatedState = update(state, {
        boardData: {
          boardContent: {
            $splice: [[oldListPos, 1]],
          },
        },
      });

      const listsLength = updatedState.boardData.boardContent.length;

      for (let i = newListPos; i < listsLength; i++) {
        updatedState = update(updatedState, {
          boardData: {
            boardContent: {
              [i]: {
                order: {
                  $set: i + 2,
                },
              },
            },
          },
        });
      }

      updatedState = update(updatedState, {
        boardData: {
          boardContent: {
            $splice: [[newListPos, 0, movedList]],
          },
        },
      });
      console.log(updatedState)

      return updatedState
    }
    default:
      return state;
  }
};

export default reducer;
