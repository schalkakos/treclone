import * as actionTypes from '../actions/actionTypes';
import update from "immutability-helper";

const initialState = {
  boards: [],
  loading: null,
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case actionTypes.GET_BOARDS: {
      return update(state, {
        loading: {$set: true},
      })
    }
    case actionTypes.SET_BOARDS: {
      return update(state, {
        loading: {$set: false},
        boards: {$set: action.responseData}
      })
    }
    case actionTypes.DELETE_BOARD_SUCCESS: {
      //!!
      return state
    }
    case actionTypes.CREATE_BOARD_SUCCESS: {
      console.log(action.createdBoard)
      return update(state, {
        boards: {$push: [action.createdBoard]}
      })
    }
    default: {
      return state
    }
  }
}

export default reducer;