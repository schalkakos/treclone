import * as actionTypes from '../actions/actionTypes';
import update from "immutability-helper";

const initialState = {
  userId: null,
  token: null,
  error: null,
  path: "/",
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SIGNIN_START: {
      return update(state, {
        loading: {$set: true},
      })
    }
    case actionTypes.SIGNIN_SUCCESS: {
      return update(state, {
        userId: {$set: action.userId},
        token: {$set: action.token},
        loading: {$set: false},
        error: {$set: false}
      });
    }
    case actionTypes.SIGNIN_FAIL: {
      return update(state, {
        loading: {$set: false},
        error: {$set: action.error}
      });
    }
    case actionTypes.SIGNUP_FAIL: {
      return update(state, {
        error: {$set: action.error}
      })
    }
    case actionTypes.SIGNUP_SUCCESS: {
      return update(state, {
        error: {$set: false}
      })
    }
    case actionTypes.SET_REDIRECT_PATH: {
      return update(state, {
        path: {$set: action.path}
      })
    }
    case actionTypes.LOGOUT_SUCCESS: {
      return update(state, {
        userId: {$set: null},
        token: {$set: null},
        loading: {$set: false},
        error: {$set: null}
      })
    }
    default: {
      return state;
    }
  }
}

export default reducer;