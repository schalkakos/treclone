import update from "immutability-helper";

export function getListIndex(listArray, listId) {
  // console.log(listId)
  const indexes = {
    listIndex: null,
  };
  indexes.listIndex = listArray.findIndex((obj) => {
    return obj._id === listId;
  });
  return indexes;
}

export function getCardIndex(listArray, listId, cardId) {
  const indexes = getListIndex(listArray, listId);

  indexes.cardIndex = listArray[indexes.listIndex].cards.findIndex((obj) => {
    return obj._id === cardId;
  });
  return indexes;
}

export function updateOldListOrder(state, oldListPos, oldCardPos) {
  let updatedState = update(state, {
    boardData: {
      boardContent: {
        [oldListPos]: {
          cards: {
            $splice: [[oldCardPos, 1]],
          },
        },
      },
    },
  });

  const cardsLength =
    updatedState.boardData.boardContent[oldListPos].cards.length;
  for (let i = oldCardPos; i < cardsLength; i++) {
    updatedState = update(updatedState, {
      boardData: {
        boardContent: {
          [oldListPos]: {
            cards: {
              [i]: {
                order: { $set: i + 1 },
              },
            },
          },
        },
      },
    });
  }

  return updatedState;
}
