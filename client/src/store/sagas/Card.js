import axios from "axios";

import { all, put } from "redux-saga/effects";
import * as actions from "../actions";
import serverUrl from "./serverUrl";
import authHeader from "../axiosConfig";

export function* getCardSaga(action) {
  const { cardId, token, boardId } = action;
  // const cardId = action.cardId;
  const headers = authHeader(token, boardId);

  const response = yield axios
    .get(serverUrl + `/card/${cardId}`, headers)
    .catch((error) => {
      console.log(error);
      //--error handling
    });
  yield put(actions.setCard(response.data.response));
}

export function* updateCheclistNameSaga(action) {
  const { cardId, name, token, boardId } = action;
  // const cardId = action.cardId;
  // const name = action.name;
  const headers = authHeader(token, boardId);

  const response = yield axios.patch(
    serverUrl + `/card/`,
    {
      _id: cardId,
      checklistName: name,
      checklist: true,
    },
    headers
  );
  if (response.status === 200) {
    yield put(actions.updateChecklistNameSuccess(cardId, name));
  }
}

export function* createChecklistItemSaga(action) {
  const { cardId, checklistItemName, token, boardId } = action;
  // const cardId = action.cardId;
  // const checklistItemName = action.checklistItemName;
  const headers = authHeader(token, boardId);

  const response = yield axios.post(
    serverUrl + "/checklist/",
    {
      cardId: cardId,
      name: checklistItemName,
    },
    headers
  );

  yield put(actions.createChecklistItemSuccess(response.data.response));
}

export function* updateChecklistItemNameSaga(action) {
  const { checklistItemId, checklistItemName, token, boardId } = action;
  // const checklistItemId = action.checklistItemId;
  // const checklistItemName = action.checklistItemName;
  const headers = authHeader(token, boardId);

  const response = yield axios.patch(
    serverUrl + "/checklist/",
    {
      _id: checklistItemId,
      name: checklistItemName,
    },
    headers
  );

  if (response.status === 200) {
    yield put(
      actions.updateChecklistItemNameSuccess(checklistItemId, checklistItemName)
    );
  }
}

export function* updateTaskCompletionSaga(action) {
  const { checklistItemId, completionState, token, boardId } = action;
  // const checklistItemId = action.checklistItemId;
  // const completionState = action.completionState;
  const headers = authHeader(token, boardId);

  const response = yield axios.patch(
    serverUrl + "/checklist/",
    {
      _id: checklistItemId,
      done: completionState,
    },
    headers
  );

  if (response.status === 200) {
    yield put(
      actions.updateTaskCompletionSuccess(checklistItemId, completionState)
    );
  }
}

export function* deleteChecklistItemSaga(action) {
  const { checklistItemId, token, boardId } = action;
  // const checklistItemId = action.checklistItemId;
  const headers = authHeader(token, boardId);

  // const response = yield axios({
  //   method: "DELETE",
  //   url: serverUrl + `/checklist/${checklistItemId}`
  //   data: {
  //     _id: checklistItemId,
  //   },
  // });
  const response = yield axios.delete(
    serverUrl + `/checklist/${checklistItemId}`,
    headers
  );

  if (response.status === 200) {
    yield put(actions.deleteChecklistItemSuccess(checklistItemId));
  }
}

function updateCard(cardId, headers) {
  console.log("updateCard")
  return axios.patch(
    serverUrl + "/card/",
    {
      _id: cardId,
      checklist: false,
      checklistName: "",
    },
    headers
  );
}

function deleteChecklistItems(cardId, headers) {
  console.log("deleteChecklistItems");
  // console.log(headers.headers.Authorization)
  // console.log(
  //   axios({
  //     method: "DELETE",
  //     headers: {Authorization: headers.headers.Authorization},
  //     url: serverUrl + "/checklist",
  //     data: {
  //       cardId: cardId,
  //     },
  //   })
  // );
  return axios({
    method: "DELETE",
    headers: {Authorization: headers.headers.Authorization, BoardId: headers.headers.BoardId},
    url: serverUrl + "/checklist",
    data: {
      cardId: cardId,
    },
  });
}

export function* deleteChecklistSaga(action) {
  const { cardId, token, boardId } = action;
  console.log(boardId)
  const headers = authHeader(token, boardId);
  // console.log(headers);
  const [cardUpdateResponse, checklistItemDeleteResponse] = yield all([
    updateCard(cardId, headers),
    deleteChecklistItems(cardId, headers),
  ]);

  if (
    cardUpdateResponse.status === 200 &&
    checklistItemDeleteResponse.status === 200
  ) {
    yield put(actions.deleteChecklistSuccess());
  }
}

export function* updateDescriptionSaga(action) {
  const { cardId, updatedDescription, token, boardId } = action;
  const headers = authHeader(token, boardId);
  console.log("update descrtiption saga");
  const response = yield axios.patch(
    serverUrl + "/card",
    {
      _id: cardId,
      description: updatedDescription,
    },
    headers
  );

  if (response.status === 200) {
    yield put(actions.updateDescriptionSuccess(action.updatedDescription));
  }
}

export function* updateCardTitleSaga(action) {
  const { cardId, title, token, boardId } = action;
  const headers = authHeader(token, boardId);

  const response = yield axios.patch(
    serverUrl + "/card",
    {
      _id: cardId,
      title: title,
    },
    headers
  );

  if (response.status === 200) {
    yield put(
      actions.updateCardTitleSuccess(action.cardId, action.listId, action.title)
    );
  }
}

export function* deleteCardSaga(action) {
  const { cardId, token, boardId } = action;
  const headers = authHeader(token, boardId);

  const response = yield axios(
    {
      method: "DELETE",
      headers: {Authorization: headers.headers.Authorization, BoardId: headers.headers.BoardId},
      url: serverUrl + "/card",
      data: {
        cardId: cardId,
      },
    },
  );

  if (response.status === 200) {
    yield put(actions.deleteCardSuccess(action.cardId, action.listId));
  }
}

export function* updateDueDateSaga(action) {
  const { dueDate, cardId, token, boardId } = action;
  // console.log(token)
  const headers = authHeader(token, boardId);

  let response = null;
  if (dueDate === null) {
    response = yield axios.patch(
      serverUrl + "/card",
      {
        _id: cardId,
        dueDate: dueDate,
        dueDateDone: false,
      },
      headers
    );
  } else {
    response = yield axios.patch(
      serverUrl + "/card",
      {
        _id: cardId,
        dueDate: dueDate,
      },
      headers
    );
  }

  if (response.status === 200) {
    yield put(
      actions.updateDueDateSuccess(action.cardId, action.listId, action.dueDate)
    );
  }
}

export function* updateDueDateDoneSaga(action) {
  const { cardId, listId, dueDateDone, token, boardId } = action;
  const headers = authHeader(token, boardId);

  const response = yield axios.patch(
    serverUrl + "/card",
    {
      _id: cardId,
      dueDateDone: dueDateDone,
    },
    headers
  );

  if (response.status === 200) {
    yield put(actions.updateDueDateDoneSuccess(cardId, listId, dueDateDone));
  }
}
