import axios from "axios";

import { put } from "redux-saga/effects";
import * as actions from "../actions";
import serverUrl from "./serverUrl";
import authHeader from "../axiosConfig";

export function* getBoardSaga(action) {
  try {
    const { boardId, token } = action;
    const headers = authHeader(token, boardId);
    localStorage.setItem("boardId", boardId);

    const response = yield axios.get(serverUrl + `/board/${boardId}`, headers);
    // .catch((error) => {
    //   console.log(error);
    //   //--error handling
    // });

    yield put(actions.setBoard(response.data.result));
  } catch (error) {
    const errorStatus = error.response.status;

    if (errorStatus === 401) {
      const userId = localStorage.getItem("userId");
      const { history } = action;

      history.push(`/${userId}/boards`);
      return;
    }
    console.log("not handled error in getBoards with status: ", errorStatus);
  }
}

export function* addCardSaga(action) {
  const { title, listId, order, token, boardId } = action;
  const headers = authHeader(token, boardId);
  // const title = action.title;
  // const listId = action.listId;
  // const order = action.order;
  // const token

  const response = yield axios
    .post(
      serverUrl + "/card/",
      {
        title: title,
        listId: listId,
        description: "",
        order: order,
        members: [],
      },
      headers
    )
    .catch((error) => {
      console.log(error);
      //--error handling
    });
  yield put(actions.addCardSuccess(response.data.response));
}

export function* updateListTitleSaga(action) {
  const { listId, title, token, boardId } = action;
  const headers = authHeader(token, boardId);

  // const _id = action.listId;
  // const title = action.title;

  const response = yield axios.patch(
    serverUrl + "/list/",
    {
      _id: listId,
      title: title,
    },
    headers
  );

  if (response.status === 200) {
    yield put(actions.updateListTitleSuccess(listId, title));
  }
}

export function* updateListOrderSaga(action) {
  const { order, _id, token, boardId } = action;
  const headers = authHeader(token, boardId);
  // const order = action.order;
  // const _id = action.listId;

  const response = yield axios.patch(
    serverUrl + "/list/",
    {
      order: order,
      _id: _id,
    },
    headers
  );

  if (response.status === 200) {
    yield put(actions.updateListOrderSuccess(_id, order));
  }
}

export function* updateListSortSaga(action) {
  const { listId, sortBy, token, boardId } = action;
  const headers = authHeader(token, boardId);
  // const _id = action.listId;
  // const sortBy = action.sortBy;

  const response = yield axios.patch(
    serverUrl + "/list/",
    {
      _id: listId,
      sortBy: sortBy,
    },
    headers
  );
  console.log(response);

  if (response.status === 200) {
    yield put(actions.updateListSortSuccess(listId, sortBy));
  }
}

export function* createListSaga(action) {
  const { boardId, order, title, token } = action;
  const headers = authHeader(token, boardId);

  const response = yield axios.post(
    serverUrl + "/list",
    {
      boardId: boardId,
      order: order,
      title: title,
    },
    headers
  );
  // console.log(response.data.result)
  yield put(actions.createListSuccess(response.data.result));
}

export function* deleteListSaga(action) {
  const { listId, token, boardId } = action;
  const headers = authHeader(token, boardId);

  const response = yield axios({
    method: "DELETE",
    headers: {
      Authorization: headers.headers.Authorization,
      BoardId: headers.headers.BoardId,
    },
    url: serverUrl + "/list",
    data: {
      listId: listId,
    },
  });

  if (response.status === 200 || response.status === 404) {
    yield put(actions.deleteListSuccess(action.listId));
  }
}

export function* moveListSaga(action) {
  const { oldListPos, newListPos, listId, boardId, token } = action;
  const headers = authHeader(token, boardId);
  // console.log("listId", listId, boardId, "boardId")
  // console.log(action)

  const response = yield axios.patch(
    serverUrl + "/list/order/",
    {
      oldOrder: oldListPos + 1,
      newOrder: newListPos + 1,
      listId: listId,
      boardId: boardId,
    },
    headers
  );

  if (response.status === 200) {
    yield put(actions.moveListSuccess(listId, oldListPos, newListPos));
  }
}

export function* moveCardSameListSaga(action) {
  const {
    oldListPos,
    oldCardPos,
    newListPos,
    newCardPos,
    cardId,
    listId,
    token,
    boardId,
  } = action;

  const headers = authHeader(token, boardId);

  axios.patch(
    serverUrl + "/card/order/",
    {
      oldOrder: oldCardPos + 1,
      newOrder: newCardPos + 1,
      cardId: cardId,
      listId: listId,
    },
    headers
  );

  axios.patch(
    serverUrl + "/list/",
    {
      _id: listId,
      sortBy: { order: 1 },
    },
    headers
  );

  yield put(
    actions.moveCardSuccess(
      listId,
      oldListPos,
      oldCardPos,
      newListPos,
      newCardPos
    )
  );
}

export function* moveCardDifferentListSaga(action) {
  const {
    oldListPos,
    oldCardPos,
    newListPos,
    newCardPos,
    cardId,
    oldListId,
    newListId,
    token,
    boardId,
  } = action;

  const headers = authHeader(token, boardId);

  axios.patch(
    serverUrl + "/card/order/",
    {
      oldOrder: oldCardPos + 1,
      newOrder: newCardPos + 1,
      cardId: cardId,
      oldListId: oldListId,
      newListId: newListId,
    },
    headers
  );

  axios.patch(
    serverUrl + "/list/",
    {
      _id: newListId,
      sortBy: { order: 1 },
    },
    headers
  );

  yield put(
    actions.moveCardSuccess(
      newListId,
      oldListPos,
      oldCardPos,
      newListPos,
      newCardPos
    )
  );
}

export function* getBoardIdFromLocalStorage(action) {
  const { token } = action;
  const boardId = yield localStorage.getItem("boardId");

  if (boardId) {
    const headers = authHeader(token, boardId);

    const response = yield axios
      .get(serverUrl + `/board/${boardId}`, headers)
      .catch((error) => {
        console.log(error);
        //--error handling
      });
    yield put(actions.setBoard(response.data.result));
  }
}
