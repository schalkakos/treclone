import axios from "axios";
import * as actions from "../actions";
import serverUrl from "./serverUrl";
import { put, delay} from "redux-saga/effects";

// export const LOGOUT = "LOGOUT";
// export const SET_REDIRECT_PATH = "SET_REDIRECT_PATH";
// export const SET_LOGOUT_TIME = "SET_LOGOUT_TIME";
// export const CHECK_AUTH = "CHECK_AUTH";

export function* signinSaga(action) {
  const { email, password } = action;

  let response = {};
  try {
    response = yield axios.post(serverUrl + "/login", {
      email: email,
      password: password,
    });

    const expiryDate = new Date(new Date().getTime() + response.data.expiresIn * 1000);
    localStorage.setItem("token", response.data.token);
    localStorage.setItem("userId", response.data.userId);
    localStorage.setItem("expiryDate", expiryDate);
    yield put(actions.setLogoutTime(response.data.expiresIn * 1000));
    yield put(actions.signinSuccess(response.data.token, response.data.userId));
  } catch (error) {
    console.log(error.response);
    yield put(actions.signinFail(error.response.data));
  }
}

export function* signupSaga(action) {
  const { email, password, confirmPassword } = action;

  try {
    /* eslint-disable no-unused-vars */

    let response = yield axios.post(serverUrl + "/signup", {
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    });
    /* eslint-enable no-unused-vars */
    yield put(actions.signupSuccess());
  } catch (error) {
    console.log(error.response);
    yield put(actions.signupFail(error.response.data));
  }
}

export function* logoutSaga() {
  localStorage.removeItem("token");
  localStorage.removeItem("userId");
  localStorage.removeItem("expiryDate");
  localStorage.removeItem("boardId");
  yield put(actions.logoutSuccess());
}

export function* setLogoutTimeSaga(action) {
  const { expiresIn } = action;

  yield delay(expiresIn);
  yield put(actions.logout());
}

export function* checkAuth() {
  const token = localStorage.getItem("token");
  if (!token) {
    yield put(actions.logout());
  } else {
    const expiryDate = new Date(localStorage.getItem("expiryDate"));
    if (expiryDate <= new Date()) {
      yield put(actions.logout());
    } else {
      const userId = localStorage.getItem("userId");
      yield put(actions.signinSuccess(token, userId));
      yield put(
        actions.setLogoutTime(expiryDate.getTime() - new Date().getTime())
      );
    }
  }
}
