import { takeEvery, takeLatest } from "redux-saga/effects";

import * as actionTypes from "../actions/actionTypes";
import {
  addCardSaga,
  getBoardSaga,
  updateListTitleSaga,
  updateListOrderSaga,
  updateListSortSaga,
  createListSaga,
  deleteListSaga,
  moveListSaga,
  moveCardSameListSaga,
  moveCardDifferentListSaga,
  getBoardIdFromLocalStorage
} from "./Board";

import {
  getCardSaga,
  updateCheclistNameSaga,
  createChecklistItemSaga,
  updateChecklistItemNameSaga,
  updateTaskCompletionSaga,
  deleteChecklistItemSaga,
  deleteChecklistSaga,
  updateDescriptionSaga,
  updateCardTitleSaga,
  deleteCardSaga,
  updateDueDateSaga,
  updateDueDateDoneSaga
} from "./Card";

import {
  signinSaga,
  signupSaga,
  logoutSaga,
  setLogoutTimeSaga,
  checkAuth,
} from './Auth';

import {
  getBoardsSaga,
  deleteBoardSaga,
  createBoardSaga
} from "./BoardSelector";

export function* watchBoard() {
  yield takeEvery(actionTypes.GET_BOARD, getBoardSaga);
  yield takeEvery(actionTypes.ADD_CARD, addCardSaga);
  yield takeEvery(actionTypes.UPDATE_LIST_TITLE, updateListTitleSaga);
  yield takeEvery(actionTypes.UPDATE_LIST_ORDER, updateListOrderSaga);
  yield takeEvery(actionTypes.UPDATE_LIST_SORT, updateListSortSaga);
  yield takeEvery(actionTypes.CREATE_LIST, createListSaga);
  yield takeEvery(actionTypes.DELETE_LIST, deleteListSaga);
  yield takeEvery(actionTypes.MOVE_LIST, moveListSaga);
  yield takeEvery(actionTypes.MOVE_CARD_SAME_LIST, moveCardSameListSaga);
  yield takeEvery(actionTypes.MOVE_CARD_DIFFERENT_LIST, moveCardDifferentListSaga);
  yield takeEvery(actionTypes.GET_BOARDID_FROM_LOCALSTORAGE, getBoardIdFromLocalStorage);
}

export function* watchCard() {
  yield takeEvery(actionTypes.GET_CARD, getCardSaga);
  yield takeEvery(actionTypes.UPDATE_CHECKLIST_NAME, updateCheclistNameSaga);
  yield takeEvery(actionTypes.CREATE_CHECKLIST_ITEM, createChecklistItemSaga);
  yield takeEvery(
    actionTypes.UPDATE_CHECKLIST_ITEM_NAME,
    updateChecklistItemNameSaga
  );
  yield takeEvery(actionTypes.UPDATE_TASK_COMPLETION, updateTaskCompletionSaga);
  yield takeEvery(actionTypes.DELETE_CHECKLIST_ITEM, deleteChecklistItemSaga);
  yield takeEvery(actionTypes.DELETE_CHECKLIST, deleteChecklistSaga);
  yield takeEvery(actionTypes.UPDATE_DESCRIPTION, updateDescriptionSaga);
  yield takeEvery(actionTypes.UPDATE_CARD_TITLE, updateCardTitleSaga);
  yield takeEvery(actionTypes.DELETE_CARD, deleteCardSaga);
  yield takeEvery(actionTypes.UPDATE_DUE_DATE, updateDueDateSaga);
  yield takeEvery(actionTypes.UPDATE_DUE_DATE_DONE, updateDueDateDoneSaga);
}

export function* watchAuth() {
  yield takeEvery(actionTypes.SIGNIN_START, signinSaga);
  yield takeEvery(actionTypes.SIGNUP, signupSaga);
  yield takeEvery(actionTypes.LOGOUT, logoutSaga);
  yield takeLatest(actionTypes.SET_LOGOUT_TIME, setLogoutTimeSaga);
  yield takeEvery(actionTypes.CHECK_AUTH, checkAuth);
}

export function* watchBoardSelector() {
  yield takeEvery(actionTypes.GET_BOARDS, getBoardsSaga);
  yield takeEvery(actionTypes.DELETE_BOARD, deleteBoardSaga);
  yield takeEvery(actionTypes.CREATE_BOARD, createBoardSaga)
}
