import axios from "axios";
import * as actions from "../actions";
import serverUrl from "./serverUrl";
import { put } from "redux-saga/effects";
import authHeader from "../axiosConfig";

export function* getBoardsSaga(action) {
  try {
    const { userId, token } = action;
    const headers = authHeader(token);

    const { data, status } = yield axios.get(
      serverUrl + `/${userId}/boards`,
      headers
    );

    if (status === 200) {
      yield put(actions.setBoards(data.boards));
    }
  } catch (error) {
    const errorStatus = error.response.status;
    
    if (errorStatus === 401) {
      const userId = localStorage.getItem("userId");
      const { history } = action;

      history.push(`/${userId}/boards`);
      return;
    }
    console.log("not handled error in getBoards with status: ", errorStatus);
  }
}

export function* createBoardSaga(action) {
  const { boardTitle, userId, token } = action;
  const headers = authHeader(token);

  const { data, status } = yield axios.post(
    serverUrl + "/board",
    {
      name: boardTitle,
      ownerId: userId,
    },
    headers
  );

  if (status === 201) {
    yield put(actions.createBoardSuccess(data.createdBoard));
  }
}

export function* deleteBoardSaga(action) {
  //   // const {boardId} = action;
}
