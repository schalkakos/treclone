export const GET_BOARD = "GET_BOARD";
export const GET_BOARD_FAIL = "GET_BOARD_FAIL";
export const SET_BOARD = "SET_BOARD";

export const ADD_CARD = "ADD_CARD";
export const ADD_CARD_SUCCESS= "ADD_CARD_SUCCESS";
export const REMOVE_CARD = "REMOVE_CARD";
export const CREATE_LIST = "CREATE_LIST";
export const CREATE_LIST_SUCCESS = "CREATE_LIST_SUCCESS";
export const UPDATE_LIST_TITLE = "UPDATE_LIST";
export const UPDATE_LIST_TITLE_SUCCESS = "UPDATE_LIST_SUCCESS";
export const UPDATE_LIST_ORDER = "UPDATE_LIST_ORDER";
export const UPDATE_LIST_ORDER_SUCCESS = "UPDATE_LIST_ORDER_SUCCESS";
export const UPDATE_LIST_SORT = "UPDATE_LIST_SORT";
export const UPDATE_LIST_SORT_SUCCESS = "UPDATE_LIST_SORT_SUCCESS";
export const DELETE_LIST = "DELETE_LIST";
export const DELETE_LIST_SUCCESS = "DELETE_LIST_SUCCESS";



export const GET_CARD = "GET_CARD";
export const SET_CARD = "SET_CARD";
export const UPDATE_CHECKLIST_NAME = "UPDATE_CHECKLISTNAME";
export const UPDATE_CHECKLIST_NAME_SUCCESS = "UPDATE_CHECKLISTNAME_SUCCESS";
export const CREATE_CHECKLIST_ITEM = 'CREATE_CHECKLIST_ITEM';
export const CREATE_CHECKLIST_ITEM_SUCCESS = 'CREATE_CHECKLIST_ITEM_SUCCESS';
export const UPDATE_CHECKLIST_ITEM_NAME = 'UPDATE_CHECKLIST_ITEM_NAME';
export const UPDATE_CHECKLIST_ITEM_NAME_SUCCESS = 'UPDATE_CHECKLIST_ITEM_NAME_SUCCESS';
export const UPDATE_TASK_COMPLETION = "UPDATE_TASK_COMPLETION";
export const UPDATE_TASK_COMPLETION_SUCCESS = "UPDATE_TASK_COMPLETION_SUCCESS";
export const DELETE_CHECKLIST_ITEM = "DELETE_CHECKLIST_ITEM";
export const DELETE_CHECKLIST_ITEM_SUCCESS = "DELETE_CHECKLIST_ITEM_SUCCESS";
export const DELETE_CHECKLIST = "DELETE_CHECKLIST"
export const DELETE_CHECKLIST_SUCCESS = "DELETE_CHECKLIST_SUCCESS";
export const UPDATE_DESCRIPTION = "UPDATE_DESCRIPTION";
export const UPDATE_DESCRIPTION_SUCCESS = "UPDATE_DESCRIPTION_SUCCESS";
export const UPDATE_CARD_TITLE = "UPDATE_TITLE";
export const UPDATE_CARD_TITLE_SUCCESS = "UPDATE_TITLE_SUCCESS";
export const DELETE_CARD = "DELETE_CARD";
export const DELETE_CARD_SUCCESS = "DELETE_CARD_SUCCESS";
export const UPDATE_DUE_DATE = "UPDATE_DUE_DATE";
export const UPDATE_DUE_DATE_SUCCESS = "UPDATE_DUE_DATE_SUCCESS";
export const UPDATE_DUE_DATE_DONE = "UPDATE_DUE_DATE_DONE";
export const UPDATE_DUE_DATE_DONE_SUCCESS = "UPDATE_DUE_DATE_DONE_SUCCESS";
export const MOVE_CARD = "MOVE_CARD";
export const MOVE_CARD_SAME_LIST = "MOVE_CARD_SAME_LIST";
export const MOVE_CARD_SAME_LIST_SUCCESS = "MOVE_CARD_SAME_LIST_SUCCESS";
export const MOVE_CARD_DIFFERENT_LIST = "MOVE_CARD_DIFFERENT_LIST";
export const MOVE_CARD_DIFFERENT_LIST_SUCCESS = "MOVE_CARD_DIFFERENT_LIST_SUCCESS";
export const MOVE_CARD_SUCCESS = "MOVE_CARD_SUCCESS";
export const MOVE_LIST = "MOVE_LIST";
export const MOVE_LIST_SUCCESS = "MOVE_LIST_SUCCESS";

export const SIGNIN_START = "SIGNIN_START";
export const SIGNIN_SUCCESS = "SIGNIN_SUCCESS";
export const SIGNIN_FAIL = "SIGNIN_FAIL";
export const SIGNUP = "SIGNUP";
export const SIGNUP_FAIL = "SIGNUP_FAIL";
export const SIGNUP_SUCCESS = "SIGNUP_SUCCESS";
export const LOGOUT = "LOGOUT";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const SET_REDIRECT_PATH = "SET_REDIRECT_PATH";
export const SET_LOGOUT_TIME = "SET_LOGOUT_TIME";
export const CHECK_AUTH = "CHECK_AUTH";

export const GET_BOARDS = "GET_BOARDS"; 
export const SET_BOARDS = "SET_BOARDS";
export const DELETE_BOARD = "DELETE_BOARD";
export const DELETE_BOARD_SUCCESS = "DELETE_BOARD_SUCCESS";
export const CREATE_BOARD = "CREATE_BOARD";
export const CREATE_BOARD_SUCCESS = "CREATE_BOARD_SUCCESS";

export const GET_BOARDID_FROM_LOCALSTORAGE = "GET_BOARDID_FROM_LOCALSTORAGE"; 

