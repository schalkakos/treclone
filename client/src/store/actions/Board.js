import * as actionTypes from "./actionTypes";

export const getBoard = (boardId, token, history) => {
  return {
    type: actionTypes.GET_BOARD,
    boardId: boardId,
    token: token,
    history: history,
  };
};

export const getBoardFail = () => {
  return { type: actionTypes.GET_BOARD_FAIL };
};

export const setBoard = (response) => {
  return {
    type: actionTypes.SET_BOARD,
    response: response,
  };
};

export const updateListTitle = (listId, updatedTitle, token, boardId) => {
  return {
    type: actionTypes.UPDATE_LIST_TITLE,
    listId: listId,
    title: updatedTitle,
    token: token,
    boardId: boardId,
  };
};

export const updateListTitleSuccess = (listId, updatedTitle) => {
  return {
    type: actionTypes.UPDATE_LIST_TITLE_SUCCESS,
    listId: listId,
    title: updatedTitle,
  };
};

export const updateListOrder = (listId, updatedOrder, token, boardId) => {
  return {
    type: actionTypes.UPDATE_LIST_ORDER,
    listId: listId,
    order: updatedOrder,
    token: token,
    boardId: boardId,
  };
};

export const updateListOrderSuccess = (listId, updatedOrder) => {
  return {
    type: actionTypes.UPDATE_LIST_ORDER_SUCCESS,
    listId: listId,
    order: updatedOrder,
  };
};

export const updateListSort = (listId, updatedSort, token, boardId) => {
  return {
    type: actionTypes.UPDATE_LIST_SORT,
    listId: listId,
    sortBy: updatedSort,
    token: token,
    boardId: boardId,
  };
};

export const updateListSortSuccess = (listId, updatedOrder) => {
  return {
    type: actionTypes.UPDATE_LIST_SORT_SUCCESS,
    listId: listId,
    sortBy: updatedOrder,
  };
};

export const createList = (title, order, boardId, token) => {
  return {
    type: actionTypes.CREATE_LIST,
    title: title,
    order: order,
    boardId: boardId,
    token: token,
  };
};

export const createListSuccess = (response) => {
  return {
    type: actionTypes.CREATE_LIST_SUCCESS,
    response: response,
  };
};

export const deleteList = (listId, token, boardId) => {
  return {
    type: actionTypes.DELETE_LIST,
    listId: listId,
    token: token,
    boardId: boardId,
  };
};

export const deleteListSuccess = (listId) => {
  return {
    type: actionTypes.DELETE_LIST_SUCCESS,
    listId: listId,
  };
};

export const addCard = (title, listId, order, token, boardId) => {
  return {
    type: actionTypes.ADD_CARD,
    title: title,
    listId: listId,
    order: order,
    token: token,
    boardId: boardId,
  };
};

export const addCardSuccess = (response) => {
  return {
    type: actionTypes.ADD_CARD_SUCCESS,
    response: response,
  };
};

export const moveCard = (
  oldListPos,
  oldCardPos,
  newListPos,
  newCardPos,
  boardId
) => {
  return {
    type: actionTypes.MOVE_CARD,
    oldListPos: oldListPos,
    oldCardPos: oldCardPos,
    newListPos: newListPos,
    newCardPos: newCardPos,
    boardId: boardId,
  };
};

export const moveCardSameList = (
  oldListPos,
  oldCardPos,
  newListPos,
  newCardPos,
  cardId,
  listId,
  token,
  boardId
) => {
  return {
    type: actionTypes.MOVE_CARD_SAME_LIST,
    oldListPos: oldListPos,
    oldCardPos: oldCardPos,
    newListPos: newListPos,
    newCardPos: newCardPos,
    cardId: cardId,
    listId: listId,
    token: token,
    boardId: boardId,
  };
};

// export const moveCardSameListSuccess = (oldListPos, oldCardPos, newListPos, newCardPos, listId, boardId) => {
//   return {
//     type: actionTypes.MOVE_CARD_SAME_LIST_SUCCESS,
//     oldListPos: oldListPos,
//     oldCardPos: oldCardPos,
//     newListPos: newListPos,
//     newCardPos: newCardPos,
//   }
// }

export const moveCardDifferentList = (
  oldListPos,
  oldCardPos,
  newListPos,
  newCardPos,
  cardId,
  oldListId,
  newListId,
  token,
  boardId
) => {
  return {
    type: actionTypes.MOVE_CARD_DIFFERENT_LIST,
    oldListPos: oldListPos,
    oldCardPos: oldCardPos,
    newListPos: newListPos,
    newCardPos: newCardPos,
    cardId: cardId,
    oldListId: oldListId,
    newListId: newListId,
    token: token,
    boardId: boardId,
  };
};

export const moveCardSuccess = (
  listId,
  oldListPos,
  oldCardPos,
  newListPos,
  newCardPos
) => {
  return {
    type: actionTypes.MOVE_CARD_SUCCESS,
    listId: listId,
    oldListPos: oldListPos,
    oldCardPos: oldCardPos,
    newListPos: newListPos,
    newCardPos: newCardPos,
  };
};

// export const moveCardDifferentListSuccess = (oldListPos, oldCardPos, newListPos, newCardPos, oldListId, newListId, boardId) => {
//   return {
//     type: actionTypes.MOVE_CARD_DIFFERENT_LIST_SUCCESS,
//     oldListPos: oldListPos,
//     oldCardPos: oldCardPos,
//     newListPos: newListPos,
//     newCardPos: newCardPos,
//   }
// }

export const moveList = (oldListPos, newListPos, listId, boardId) => {
  return {
    type: actionTypes.MOVE_LIST,
    oldListPos: oldListPos,
    newListPos: newListPos,
    listId: listId,
    boardId: boardId,
  };
};

export const moveListSuccess = (oldListPos, newListPos) => {
  return {
    type: actionTypes.MOVE_LIST_SUCCESS,
    oldListPos: oldListPos,
    newListPos: newListPos,
  };
};

export const getBoardIdFromLocalStorage = (token) => {
  return {
    type: actionTypes.GET_BOARDID_FROM_LOCALSTORAGE,
    token: token,
  };
};
