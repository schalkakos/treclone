import * as actionTypes from "./actionTypes";

export const signinStart = (email, password) => {
  return {
    type: actionTypes.SIGNIN_START,
    email: email,
    password: password,
  }
};

export const signinSuccess = (token, userId) => {
  return {
    type: actionTypes.SIGNIN_SUCCESS,
    token: token,
    userId: userId
  }
} 

export const signinFail = (error) => {
  return {
    type: actionTypes.SIGNIN_FAIL,
    error: error,
  }
}

export const logout = () => {
  return {
    type: actionTypes.LOGOUT
  }
}

export const logoutSuccess = () => {
  return{
    type: actionTypes.LOGOUT_SUCCESS
  }
}

export const signup = (email, password, confirmPassword) => {
  return {
    type: actionTypes.SIGNUP,
    email: email,
    password: password,
    confirmPassword: confirmPassword
  }
}

export const signupFail = (error) => {
  return {
    type: actionTypes.SIGNUP_FAIL,
    error: error,
  }
}

export const signupSuccess = () => {
  return {
    type: actionTypes.SIGNIN_SUCCESS,
  }
}

export const setRedirectPath = (path) => {
  return {
    type: actionTypes.SET_REDIRECT_PATH,
    path: path
  }
}

export const setLogoutTime = (expiresIn) => {
  return {
    type: actionTypes.SET_LOGOUT_TIME,
    expiresIn: expiresIn
  }
}

export const checkAuth = () => {
  return {
    type: actionTypes.CHECK_AUTH,
  }
}