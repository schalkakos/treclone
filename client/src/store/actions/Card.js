import * as actionTypes from "./actionTypes";

export const getCard = (cardId, token, boardId) => {
  return {
    type: actionTypes.GET_CARD,
    cardId: cardId,
    token: token,
    boardId: boardId
  };
};

export const setCard = (response) => {
  return {
    type: actionTypes.SET_CARD,
    response: response,
  };
};

export const updateChecklistName = (cardId, name, token, boardId) => {
  return {
    type: actionTypes.UPDATE_CHECKLIST_NAME,
    name: name,
    cardId: cardId,
    token: token,
    boardId: boardId
  };
};

export const updateChecklistNameSuccess = (cardId, name) => {
  return {
    type: actionTypes.UPDATE_CHECKLIST_NAME_SUCCESS,
    name: name,
    cardId: cardId,
  };
};

export const createChecklistItem = (cardId, checklistItemName, token, boardId) => {
  return {
    type: actionTypes.CREATE_CHECKLIST_ITEM,
    cardId: cardId,
    checklistItemName: checklistItemName,
    token: token,
    boardId: boardId
  };
};

export const createChecklistItemSuccess = (response) => {
  return {
    type: actionTypes.CREATE_CHECKLIST_ITEM_SUCCESS,
    response: response,
  };
};

export const updateChecklistItemName = (
  checklistItemId,
  checklistItemName,
  token,
  boardId
) => {
  return {
    type: actionTypes.UPDATE_CHECKLIST_ITEM_NAME,
    checklistItemId: checklistItemId,
    checklistItemName: checklistItemName,
    token: token,
    boardId: boardId
  };
};

export const updateChecklistItemNameSuccess = (
  checklistItemId,
  checklistItemName
) => {
  return {
    type: actionTypes.UPDATE_CHECKLIST_ITEM_NAME_SUCCESS,
    checklistItemId: checklistItemId,
    checklistItemName: checklistItemName,
  };
};

export const updateTaskCompletion = (
  checklistItemId,
  completionState,
  token,
  boardId
) => {
  return {
    type: actionTypes.UPDATE_TASK_COMPLETION,
    checklistItemId: checklistItemId,
    completionState: completionState,
    token: token,
    boardId: boardId
  };
};

export const updateTaskCompletionSuccess = (
  checklistItemId,
  completionState
) => {
  return {
    type: actionTypes.UPDATE_TASK_COMPLETION_SUCCESS,
    checklistItemId: checklistItemId,
    completionState: completionState,
  };
};

export const deleteChecklistItem = (checklistItemId, token, boardId) => {
  return {
    type: actionTypes.DELETE_CHECKLIST_ITEM,
    checklistItemId: checklistItemId,
    token: token,
    boardId: boardId
  };
};

export const deleteChecklistItemSuccess = (checklistItemId) => {
  return {
    type: actionTypes.DELETE_CHECKLIST_ITEM_SUCCESS,
    checklistItemId: checklistItemId,
  };
};

export const deleteChecklist = (cardId, token, boardId) => {
  return {
    type: actionTypes.DELETE_CHECKLIST,
    cardId: cardId,
    token: token,
    boardId: boardId
  };
};

export const deleteChecklistSuccess = () => {
  return {
    type: actionTypes.DELETE_CHECKLIST_SUCCESS,
  };
};

export const updateDescription = (cardId, description, token, boardId) => {
  return {
    type: actionTypes.UPDATE_DESCRIPTION,
    cardId: cardId,
    updatedDescription: description,
    token: token,
    boardId: boardId
  };
};

export const updateDescriptionSuccess = (description) => {
  return {
    type: actionTypes.UPDATE_DESCRIPTION_SUCCESS,
    updatedDescription: description,
  };
};

export const updateCardTitle = (cardId, listId, updatedTitle, token, boardId) => {
  return {
    type: actionTypes.UPDATE_CARD_TITLE,
    cardId: cardId,
    listId: listId,
    title: updatedTitle,
    token: token,
    boardId: boardId
  };
};

export const updateCardTitleSuccess = (cardId, listId, updatedTitle) => {
  return {
    type: actionTypes.UPDATE_CARD_TITLE_SUCCESS,
    cardId: cardId,
    listId: listId,
    updatedTitle: updatedTitle,
  };
};

export const deleteCard = (cardId, listId, token, boardId) => {
  return {
    type: actionTypes.DELETE_CARD,
    cardId: cardId,
    listId: listId,
    token: token,
    boardId: boardId
  };
};

export const deleteCardSuccess = (cardId, listId) => {
  return {
    type: actionTypes.DELETE_CARD_SUCCESS,
    cardId: cardId,
    listId: listId,
  };
};

export const updateDueDate = (cardId, listId, updatedDueDate, token, boardId) => {
  return {
    type: actionTypes.UPDATE_DUE_DATE,
    cardId: cardId,
    dueDate: updatedDueDate,
    listId: listId,
    token: token,
    boardId: boardId
  };
};

export const updateDueDateSuccess = (cardId, listId, updatedDueDate) => {
  return {
    type: actionTypes.UPDATE_DUE_DATE_SUCCESS,
    cardId: cardId,
    listId: listId,
    dueDate: updatedDueDate,
  };
};

export const updateDueDateDone = (
  cardId,
  listId,
  updatedDueDateDone,
  token,
  boardId
) => {
  return {
    type: actionTypes.UPDATE_DUE_DATE_DONE,
    cardId: cardId,
    listId: listId,
    dueDateDone: updatedDueDateDone,
    token: token,
    boardId: boardId
  };
};

export const updateDueDateDoneSuccess = (
  cardId,
  listId,
  updatedDueDateDone
) => {
  return {
    type: actionTypes.UPDATE_DUE_DATE_DONE_SUCCESS,
    cardId: cardId,
    listId: listId,
    dueDateDone: updatedDueDateDone,
  };
};
