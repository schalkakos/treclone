import * as actionTypes from "./actionTypes";

export const getBoards = (userId, token, history) => {
  return {
    type: actionTypes.GET_BOARDS,
    userId: userId,
    token: token,
    history: history
  };
};

export const setBoards = (responseData) => {
  return {
    type: actionTypes.SET_BOARDS,
    responseData: responseData,
  };
};

export const createBoard = (boardTitle, userId, token) => {
  return {
    type: actionTypes.CREATE_BOARD,
    boardTitle: boardTitle,
    userId: userId,
    token: token,
  };
};

export const createBoardSuccess = (createdBoard) => {
  return {
    type: actionTypes.CREATE_BOARD_SUCCESS,
    createdBoard: createdBoard,
  };
};
