const authHeader = (token, boardId = null) => {
  return boardId
    ? {
        headers: {
          Authorization: "Bearer " + token,
          BoardId: boardId,
        },
      }
    : {
        headers: {
          Authorization: "Bearer " + token,
        },
      }
};

export default authHeader;
