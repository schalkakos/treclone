import React from "react";
import { withRouter } from "react-router-dom";

import styles from "./Backdrop.module.css";

const Backdrop = (props) => {
  const mousedownHandler = (event) => {
    props.history.goBack();
  };

  return <div className={styles.Backdrop} onClick={mousedownHandler}></div>;
};

export default withRouter(Backdrop);
