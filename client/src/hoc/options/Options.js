import React, { useEffect } from "react";
import scrollIntoView from "scroll-into-view-if-needed";

import styles from "./Options.module.css";
import ListOptionStyles from "../../components/Lists/List/ListOptions/ListOption.module.css";

const Option = (props) => {
  const outsideClickHandler = (event) => {
    const materialUiDropdown = document.querySelector("div.MuiPaper-root");
    if (event.button === 0) {
      if (
        props.optionsRef.current.contains(event.target) ||
        props.optionsRef.current.previousElementSibling.contains(event.target)
      ) {
        return;
      } else if (materialUiDropdown) {
        //this is needed because materialUiDropdown is undefined unless it's in the dom and the check below throws an error whenever the dropdown is not present
        if (materialUiDropdown.contains(event.target)) {
          return;
        }
      }
      props.showOptions();
    }
  };

  useEffect(() => {
    if (props.scroll) {
      // console.log(props.scroll)
      const optionElement = document.querySelector(
        "div." + ListOptionStyles.Option
      );
      scrollIntoView(optionElement, { scrollMode: "if-needed" });
    }
  });

  useEffect(() => {
    document.addEventListener("mousedown", outsideClickHandler);
    return () => {
      document.removeEventListener("mousedown", outsideClickHandler);
    };
  });

  return (
    <div ref={props.optionsRef} className={styles.OptionWrapper}>
      {props.children}
    </div>
  );
};

export default Option;
