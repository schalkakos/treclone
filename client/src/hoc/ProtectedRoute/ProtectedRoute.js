import React from "react";
import { Route, Redirect, useLocation } from "react-router-dom";
import { connect } from "react-redux";

import * as actions from "../../store/actions";

const ProtectedRoute = (props) => {
  const {
    loading,
    setRedirectPath,
    isLoggedIn,
    modal,
    component: Component,
    ...prop
  } = props;
  const location = useLocation();

  if (location.pathname !== "/" && !isLoggedIn) {
    // setRedirectPath(location.pathname);
  }
  return (
    <Route
      {...prop}
      render={(props) => {
        if (loading === false) {
          return isLoggedIn ? (
            <Component {...props} modal={modal} />
          ) : (
            <Redirect to="/login" />
          );
        }
      }}
    />
  );
};

const mapStateToProps = (state) => {
  return {
    authState: state.auth,
    isLoggedIn: state.auth.token ? true : false,
    loading: state.auth.loading,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setRedirectPath: (path) => dispatch(actions.setRedirectPath(path)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedRoute);
