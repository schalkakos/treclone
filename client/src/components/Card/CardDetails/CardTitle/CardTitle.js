import React, { useState } from "react";

import styles from "./CardTitle.module.css";
import TextAreaAutosize from "react-textarea-autosize";
import { IconContext } from "react-icons";
import { BsKanbanFill } from "react-icons/bs";

const CardTitle = (props) => {
  const [cardTitle, setCardTitle] = useState(props.cardTitle);

  const titleChangeHandler = (event) => {
    setCardTitle(event.target.value);
  };

  const titleBlurHandler = (event) => {
    const updatedTitle = event.target.value;
    if (props.cardTitle !== updatedTitle) {
      props.updateTitle(props.cardId, props.listId, updatedTitle, props.token, props.boardId);
    }
  };

  return (
    <div className={styles.CardTitle}>
      <div>
        <IconContext.Provider value={{ size: "25px" }}>
          <BsKanbanFill />
        </IconContext.Provider>
      </div>
      <TextAreaAutosize
        value={cardTitle}
        onChange={titleChangeHandler}
        onBlur={titleBlurHandler}
      />
    </div>
  );
};

export default CardTitle;
