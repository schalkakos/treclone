import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";

import * as actions from "../../../../store/actions/";
import styles from "./CardActions.module.css";
import { IconContext } from "react-icons";
import { BsArrowRight, BsClock, BsListCheck, BsTrash } from "react-icons/bs";
import CardActionChecklist from "./CardActionItems/CardActionChecklist";
import CardActionMove from "./CardActionItems/CardActionMove";

const ActionButtons = (props) => {
  const [modalDispalyed, setModalDisplayed] = useState("");
  const { onDeleteCard, onUpdateDueDate, boardId, cardContent, token } = props;

  const buttonClickHandler = (value) => {
    if (value !== modalDispalyed) {
      setModalDisplayed(value);
    } else {
      setModalDisplayed("");
    }
  };

  const deleteCardClickHandler = (event) => {
    if (event.type === "click") {
      onDeleteCard(cardContent._id, cardContent.listId, token, boardId);
    } else if (event.key === "Enter") {
      onDeleteCard(cardContent._id, cardContent.listId, token, boardId);
    }
  };

  const updateDueDate = (dueDate) => {
    onUpdateDueDate(cardContent._id, cardContent.listId, dueDate, token, boardId);
  };

  // let moveModal = null;
  // let dueDateModal = null;
  // let checklistModal = null;
  let modal = null;
  switch (modalDispalyed) {
    case "move":
      modal = (
        <CardActionMove
          cancelModal={setModalDisplayed}
          cardId={props.cardContent._id}
          listId={props.cardContent.listId}
        />
      );
      break;
    case "due date":
      modal = (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DateTimePicker
            ampm={false}
            open={true}
            disablePast={true}
            value={cardContent.dueDate ? cardContent.dueDate : null}
            onClose={() => {
              setModalDisplayed("");
            }}
            onChange={updateDueDate}
            TextFieldComponent={() => null}
            clearable={true}
            clearLabel="Remove"
            okLabel="Save"
          />
        </MuiPickersUtilsProvider>
      );
      break;
    case "checklist":
      modal = <CardActionChecklist setDisplayedModal={setModalDisplayed} />;
      break;
    default:
      break;
  }

  return (
    <div className={styles.CardActions}>
      <h2>Actions</h2>
      <div className={styles.CardActionButtons}>
        <div className={styles.ButtonWrapper}>
          <button
            onClick={() => {
              buttonClickHandler("move");
            }}
          >
            <IconContext.Provider value={{ size: "20px" }}>
              <BsArrowRight />
            </IconContext.Provider>
            <span>Move</span>
          </button>
          {modalDispalyed === "move" ? modal : null}
        </div>
        <div className={styles.ButtonWrapper}>
          <button
            onClick={() => {
              buttonClickHandler("due date");
            }}
          >
            <IconContext.Provider value={{ size: "20px" }}>
              <BsClock />
            </IconContext.Provider>
            <span>Due Date</span>
          </button>
          {modalDispalyed === "due date" ? modal : null}
        </div>

        <div className={styles.ButtonWrapper}>
          <button
            onClick={() => {
              buttonClickHandler("checklist");
            }}
          >
            <IconContext.Provider value={{ size: "20px" }}>
              <BsListCheck />
            </IconContext.Provider>
            <span>Checklist</span>
          </button>
          {modalDispalyed === "checklist" ? modal : null}
        </div>
        <Link to={`/board/${boardId}`}>
          <div className={styles.ButtonWrapper}>
            <button
              onClick={deleteCardClickHandler}
              onKeyDown={deleteCardClickHandler}
            >
              <IconContext.Provider value={{ size: "20px" }}>
                <BsTrash />
              </IconContext.Provider>
              <span>Delete</span>
            </button>
          </div>
        </Link>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    boardId: state.board.boardData._id,
    cardContent: state.card.cardData,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onDeleteCard: (cardId, listId, token, boardId) =>
      dispatch(actions.deleteCard(cardId, listId, token, boardId)),
    onUpdateDueDate: (cardId, listId, updatedDueDate, token, boardId) => {
      return dispatch(
        actions.updateDueDate(cardId, listId, updatedDueDate, token, boardId)
      );
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ActionButtons);
