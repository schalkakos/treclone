import React, { useState, useRef, useEffect } from "react";
import { connect } from "react-redux";
import * as actions from "../../../../../store/actions";
// import scrollIntoView from "scroll-into-view-if-needed";

import styles from "./CardAction.module.css";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";

const CardActionChecklist = (props) => {
  const [checklistName, setChecklistName] = useState(
    props.checklistName ? props.checklistName : "Checklist"
  );

  const inputRef = useRef(null);

  const checklistNameChangeHandler = (event) => {
    setChecklistName(event.target.value);
  };

  const closeButtonClickHandler = () => {
    props.setDisplayedModal("");

  };

  const successButtonClickHandler = (event) => {
    if ((event.type = "click")) {
      if (props.checklistName !== inputRef.current.value) {
        props.onUpdateChecklist(props.cardId, inputRef.current.value, props.token, props.boardId);
      }
      props.setDisplayedModal("");
    } else if ((event.key = "Enter")) {
      if (props.checklistName !== inputRef.current.value) {
        props.onUpdateChecklist(props.cardId, inputRef.current.value, props.token, props.boardId);
      }
      closeButtonClickHandler();
    }
  };

  const deleteChecklistClickHandler = (event) => {
    if (event.type === "click") {
      props.onDeleteChecklistItem(props.cardId, props.token, props.boardId);
    } else if (event.key === "Enter") {
      props.onDeleteChecklistItem(props.cardId, props.token, props.boardId);
    }
    closeButtonClickHandler()
  };


  useEffect(() => {
    inputRef.current.select();
   

  }, []);
  


  let actionButtons = null;
  if (!props.isChecklistSet) {
    actionButtons = (
      <button
        className={styles.SuccessButton}
        onClick={successButtonClickHandler}
      >
        Add
      </button>
    );
  } else {
    actionButtons = (
      <React.Fragment>
        <button
          className={styles.SuccessButton}
          onClick={successButtonClickHandler}
          onKeyDown={successButtonClickHandler}
        >
          Save
        </button>
        <button
          className={styles.DangerButton}
          onClick={deleteChecklistClickHandler}
          onKeyDown={deleteChecklistClickHandler}
        >
          Delete
        </button>
      </React.Fragment>
    );
  }


  return (
    <div className={styles.Option}>
      <div className={styles.OptionHeader}>
        <span>Add Checklist</span>
        <div
          tabIndex="0"
          className={styles.Close}
          onClick={closeButtonClickHandler}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <MdClose />
          </IconContext.Provider>
        </div>
      </div>
      <div className={styles.Input}>
        <input
          ref={inputRef}
          onChange={checklistNameChangeHandler}
          defaultValue={checklistName}
        />
      </div>
      <div className={styles.Actions}>{actionButtons}</div>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateChecklist: (cardId, checklistName, token, boardId) => {
      dispatch(actions.updateChecklistName(cardId, checklistName, token, boardId));
    },
    onDeleteChecklistItem: (cardId, token, boardId) => {
      dispatch(actions.deleteChecklist(cardId, token, boardId))
    }
  };
};

const mapStateToProps = (state) => {
  return {
    isChecklistSet: state.card.cardData.checklist,
    cardId: state.card.cardData._id,
    checklistName: state.card.cardData.checklistName,
    token: state.auth.token,
    boardId: state.board.boardData._id
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardActionChecklist);
