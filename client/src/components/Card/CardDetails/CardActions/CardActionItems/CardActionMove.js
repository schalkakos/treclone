import React, { useState } from "react";
import { connect, useSelector } from "react-redux";

import * as actions from "../../../../../store/actions";
import { makeStyles } from "@material-ui/core/styles";
import { getCardIndex } from "../../../../../store/utils";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import styles from "./CardAction.module.css";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";

//material ui css styles
const useStyles = makeStyles((theme) => ({
  listFormControl: {
    margin: theme.spacing(1),
    minWidth: 180,
    maxWidth: 180,
  },
  cardFormControl: {
    margin: theme.spacing(1),
  },
  input: {
    "& .MuiOutlinedInput-input": {
      padding: "12px 32px 12px 12px",
    },
  },
}));

const CardActionMove = (props) => {
  const {
    cardId,
    listId,
    cancelModal,
    lists,
    onMoveCardSameList,
    onMoveCardDifferentList,
    token,
    boardId,
  } = props;
  const indexes = getCardIndex(lists, listId, cardId);
  const [originalIndex] = useState(indexes);
  const [currentItems, setCurrentItems] = useState({
    currentListIndex: indexes.listIndex,
    currentListTitle: lists[indexes.listIndex].title,
    currentCardIndex: indexes.cardIndex,
  });
  const classes = useStyles();
  const newListId = useSelector(
    (state) =>
      state.board.boardData.boardContent[currentItems.currentListIndex]._id
  );

  const listSelectChangeHandler = (event) => {
    // console.log(event.target.value)
    setCurrentItems({
      currentListIndex: event.target.value,
      currentListTitle: lists[event.target.value].title,
      //if the original list is selected the set the current list index to the cards original index so the list current position is displayed in the card select
      currentCardIndex:
        event.target.value === originalIndex.listIndex
          ? originalIndex.cardIndex
          : 0,
    });
  };

  const cardSelectChangeHandler = (event) => {
    setCurrentItems((prevState) => {
      // console.log(prevState)
      return {
        currentListIndex: prevState.currentListIndex,
        currentListTitle: prevState.currentListTitle,
        currentCardIndex: event.target.value,
      };
    });
  };

  const confirmClickHandler = (event) => {
    const oldListPos = originalIndex.listIndex;
    const oldCardPos = originalIndex.cardIndex;
    const newListPos = currentItems.currentListIndex;
    const newCardPos = currentItems.currentCardIndex;

    //do smg
    if (oldListPos === newListPos && oldCardPos === newCardPos) {
      console.log("skipped card moving jazz");
      closeButtonClickHandler("");
      return;
    }
    if (
      (oldListPos === newListPos && event.type === "click") ||
      (oldListPos === newListPos && event.key === "Enter")
    ) {
      console.log("dispatch sameList");
      onMoveCardSameList(
        oldListPos,
        oldCardPos,
        newListPos,
        newCardPos,
        cardId,
        listId,
        token,
        boardId
      );
    } else {
      console.log("dispatch differentList");
      onMoveCardDifferentList(
        oldListPos,
        oldCardPos,
        newListPos,
        newCardPos,
        cardId,
        listId,
        newListId,
        token,
        boardId
      );
    }

    closeButtonClickHandler("");
  };

  const closeButtonClickHandler = () => {
    cancelModal("");
  };

  let listItems = lists.map((obj, index) => {
    // console.log(obj)
    //if the map object is the original list indicate that that's the current one else just display the title of the list
    if (originalIndex.listIndex === index) {
      return (
        <MenuItem key={index} value={index}>
          {`${obj.title} (current)`}
        </MenuItem>
      );
    } else {
      return (
        <MenuItem key={index} value={index}>
          {obj.title}
        </MenuItem>
      );
    }
  });

  let cardItems = [];

  const cards = [...lists[currentItems.currentListIndex].cards];
  //if the list doesn't have cards push a dummy object in the array, allowing you to place the card in the first position
  //if the currently selected list is not the original, push in an extra dummy object, which allows the placement of the card in the last position
  if (
    originalIndex.listIndex !== currentItems.currentListIndex ||
    cards.length === 0
  ) {
    // console.log(originalIndex.cardIndex === currentItems.currentListIndex);
    cards.push({});
  }
  cardItems = cards.map((obj, index) => {
    //if the original list is selected and the map obj is the current card indicate that that's the current one else return a normal menu item
    if (
      originalIndex.listIndex === currentItems.currentListIndex &&
      index === originalIndex.cardIndex
    ) {
      return (
        <MenuItem key={index} value={index}>{`${
          index + 1
        } (current)`}</MenuItem>
      );
    } else {
      return (
        <MenuItem key={index} value={index}>
          {index + 1}
        </MenuItem>
      );
    }
  });

  return (
    <div className={styles.Option}>
      <div className={styles.OptionHeader}>
        <span>Move Card</span>
        <div
          tabIndex="0"
          className={styles.Close}
          onClick={closeButtonClickHandler}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <MdClose />
          </IconContext.Provider>
        </div>
      </div>
      <div>
        <FormControl variant="outlined" className={classes.listFormControl}>
          <InputLabel>List</InputLabel>
          <Select
            className={classes.input}
            label="List"
            //renderValue is needed so input doesn't display the current text just the value as it's only needed in the dropdown
            renderValue={() => currentItems.currentListTitle}
            value={currentItems.currentListIndex}
            onChange={listSelectChangeHandler}
          >
            {listItems}
          </Select>
        </FormControl>
        <FormControl variant="outlined" className={classes.cardFormControl}>
          <InputLabel>Card</InputLabel>
          <Select
            className={classes.input}
            label="Card"
            //renderValue is needed so input doesn't display the current text just the value as it's only needed in the dropdown
            renderValue={() => currentItems.currentCardIndex + 1}
            value={currentItems.currentCardIndex}
            onChange={cardSelectChangeHandler}
          >
            {cardItems}
          </Select>
        </FormControl>
      </div>
      <div className={styles.Actions}>
        <button
          className={styles.SuccessButton}
          onClick={confirmClickHandler}
          onKeyDown={confirmClickHandler}
        >
          Move
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    lists: state.board.boardData.boardContent,
    token: state.auth.token,
    boardId: state.board.boardData._id,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onMoveCardDifferentList: (
      oldListPos,
      oldCardPos,
      newListPos,
      newCardPos,
      cardId,
      oldListId,
      newListId,
      token,
      boardId
    ) => {
      dispatch(
        actions.moveCardDifferentList(
          oldListPos,
          oldCardPos,
          newListPos,
          newCardPos,
          cardId,
          oldListId,
          newListId,
          token,
          boardId
        )
      );
    },
    onMoveCardSameList: (
      oldListPos,
      oldCardPos,
      newListPos,
      newCardPos,
      cardId,
      listId,
      token,
      boardId
    ) =>
      dispatch(
        actions.moveCardSameList(
          oldListPos,
          oldCardPos,
          newListPos,
          newCardPos,
          cardId,
          listId,
          token,
          boardId
        )
      ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CardActionMove);
