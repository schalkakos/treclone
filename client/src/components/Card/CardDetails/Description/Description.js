import React, { useState } from "react";
import { connect } from "react-redux";

import * as actions from "../../../../store/actions";
import styles from "./Description.module.css";
import { IconContext } from "react-icons";
import { BsJustifyLeft } from "react-icons/bs";
import EditField from "../EditField/EditField";

// props.description

const Description = (props) => {
  const [editDesc, setEditDesc] = useState(false);

  const editDescriptionHandler = () => {
    setEditDesc(true);
  };

  const cancelEditClickHandler = () => {
    setEditDesc(false);
  };

  let field = null;
  let editButton = null;
  switch (true) {
    case editDesc: {
      field = (
        <EditField
          placeholder="Add a detailed description"
          action="save"
          confirm={props.onUpdateDescription}
          cancel={cancelEditClickHandler}
          itemName={props.description}
          id={props.id}
          margin="33"
          type="desc"
        />
      );
      break;
    }
    case props.description && !editDesc: {
      editButton = (<button onClick={editDescriptionHandler}>Edit</button>)
      field = (
        <div className={styles.DescriptionField}>
          <p>{props.description}</p>
        </div>
      );
      break;
    }
    default: {
      field = (
        <textarea
          placeholder="Write a description"
          onFocus={editDescriptionHandler}
        ></textarea>
      );
      break;
    }
  }

  return (
    <div className={styles.Description}>
      <div className={styles.DescriptionHeader}>
        <IconContext.Provider value={{ size: "25px" }}>
          <BsJustifyLeft />
        </IconContext.Provider>
        <h2>Description</h2>
        {editButton}
      </div>
      {field}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    boardId: state.board.boardData._id
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateDescription: (cardId, description, token, boardId) => {
      dispatch(actions.updateDescription(cardId, description, token, boardId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Description);


// ("cardId")udpatemany({listId: listId),{})