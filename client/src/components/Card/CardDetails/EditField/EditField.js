import React, { useState, useRef } from "react";
import { connect } from "react-redux";

import styles from "./EditField.module.css";
import TextareaAutosize from "react-textarea-autosize";
import { IconContext } from "react-icons";
import { MdClose } from "react-icons/md";

const EditChecklistItem = (props) => {
  const [checklistItemName, setChecklistItemName] = useState(
    props.itemName ? props.itemName : ""
  );

  const [placeholder] = useState(props.placeholder);

  const textareaRef = useRef(null);

  const textareaChangeHandler = (event) => {
    setChecklistItemName(event.target.value);
  };

  const ConfirmButtonClickHandler = (event) => {
    const itemName = textareaRef.current.value.trim();
    if (itemName.length > 0) {
      if (event.type === "click") {
        props.confirm(props.id, itemName, props.token, props.boardId);
        props.cancel(event);
      } else if (event.key === "Enter") {
        props.confirm(props.id, itemName, props.token, props.boardId);
        props.cancel(event);
      }
    } else if (props.type === "desc") {
      props.confirm(props.id, itemName, props.token, props.boardId);
      props.cancel(event);
    }
  };

  const cancelClickHandler = (event) => {
    if (event.type === "click") {
      props.cancel(event);
    } else if (event.key === "Enter") {
      props.cancel(event);
    }
  };

  const textareaOnFocusHandler = (event) => {
    const textareaValue = event.target.value;
    event.target.value = "";
    event.target.value = textareaValue;
    event.target.focus();
  };

  return (
    <div
      style={{ marginLeft: `${props.margin}px` }}
      className={styles.EditChecklistItem}
    >
      <TextareaAutosize
        autoFocus
        ref={textareaRef}
        value={checklistItemName}
        minRows={2}
        placeholder={placeholder}
        onChange={textareaChangeHandler}
        onFocus={textareaOnFocusHandler}
      />
      <div>
        <button
          className={styles.SaveEdit}
          onClick={ConfirmButtonClickHandler}
          onKeyDown={ConfirmButtonClickHandler}
        >
          {props.action}
        </button>
        <button
          className={styles.CancelEdit}
          onClick={cancelClickHandler}
          onKeyDown={cancelClickHandler}
        >
          <IconContext.Provider value={{ size: "26px" }}>
            <MdClose />
          </IconContext.Provider>
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    boardId: state.board.boardData._id,
    cardId: state.card.cardData._id,
    token: state.auth.token,
  };
};

export default connect(mapStateToProps)(EditChecklistItem);
