import React from 'react';

import styles from './DueDate.module.css';
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { IconContext } from "react-icons";
import { BsCheck } from "react-icons/bs";



const DueDate = (props) => {
  const {markAsDone, changeDate, dueDate, dueDateDone} = props;

  return (
    <div
      className={
        dueDateDone
          ? [styles.DueDate, styles.DueDateDone].join(" ")
          : styles.DueDate
      }
    >
      <div
        className={styles.Checkbox}
        onClick={markAsDone}
        tabIndex="0"
      >
        {dueDateDone ? (
          <IconContext.Provider value={{ size: "20px" }}>
            <BsCheck />
          </IconContext.Provider>
        ) : null}
      </div>
      {dueDate ? (
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <DateTimePicker
            ampm={false}
            disablePast={true}
            value={dueDate}
            format="MMM dd yyyy HH:mm"
            onChange={changeDate}
            TextFieldComponent={(props) => (
              <button onClick={props.onClick}>{props.value}</button>
            )}
            clearable={true}
            clearLabel="Remove"
            okLabel="Save"
          />
        </MuiPickersUtilsProvider>
      ) : null}
    </div>
  )
}

export default DueDate;