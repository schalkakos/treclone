import React from "react";
import { connect } from "react-redux";

import * as actions from "../../../../../store/actions";
import { FiTrash2 } from "react-icons/fi";
import { BsCheck } from "react-icons/bs";
import { IconContext } from "react-icons/lib";
import styles from "./ChecklistItem.module.css";
import EditField from "../../EditField/EditField";

const ChecklistItem = (props) => {
  let checklistItemClass = styles.ChecklistItem;
  let checkIcon = null;

  const editHandler = (event) => {
    if (event.type === "click") {
      props.editTask({
        isEditing: true,
        editingId: props.content._id,
      });
      props.addChecklistItem(false);
    } else {
      if (event.key === "Enter") {
        props.editTask({
          isEditing: true,
          editingId: props.content._id,
        });
        props.addChecklistItem(false);
      }
    }
  };

  const cancelEditHandler = (event) => {
    if (event.type === "click") {
      props.editTask({
        isEditing: false,
        editingId: null,
      });
    } else {
      if (event.key === "Enter") {
        console.log("enter pressed");
        props.editTask({
          isEditing: false,
          editingId: null,
        });
      }
    }
  };

  const updateTaskCompletionClickHandler = (event) => {
    const updatedCompletionState = !props.content.done;

    if (event.type === "click") {
      props.onUpdateTaskCompletion(
        props.content._id,
        updatedCompletionState,
        props.token,
        props.boardId
      );
    } else if (event.key === "Enter") {
      props.onUpdateTaskCompletion(
        props.content._id,
        updatedCompletionState,
        props.token,
        props.boardId
      );
    }
  };

  const deleteChecklistItemClickHandler = (event) => {
    if (event.type === "click") {
      props.onDeleteChecklistItem(
        props.content._id,
        props.token,
        props.boardId
      );
    } else if (event.key === "Enter") {
      props.onDeleteChecklistItem(
        props.content._id,
        props.token,
        props.boardId
      );
    }
  };

  let content = (
    <div
      className={styles.ChecklistItemContent}
      onClick={editHandler}
      onKeyDown={editHandler}
      tabIndex="0"
    >
      <span>{props.content.name}</span>
      <button
        className={styles.Delete}
        onClick={(event) => {
          event.stopPropagation();
          deleteChecklistItemClickHandler(event);
        }}
        onKeyDown={(event) => {
          event.stopPropagation();
          deleteChecklistItemClickHandler(event);
        }}
      >
        <IconContext.Provider value={{ size: "20px" }}>
          <FiTrash2 />
        </IconContext.Provider>
      </button>
    </div>
  );

  if (props.isEditing === true) {
    content = (
      // <EditField itemId={props.content._id} itemName={props.content.name} type="edit" cancelEdit={cancelEditHandler} />
      <EditField
        id={props.content._id}
        itemName={props.content.name}
        action="Save"
        cancel={cancelEditHandler}
        confirm={props.onUpdateChecklistItemName}
        placeholder="Give the item a name"
        margin="12"
      />
    );
  }

  if (props.content.done) {
    checklistItemClass += ` ${styles.TaskDone}`;
    checkIcon = (
      <IconContext.Provider value={{ size: "20px" }}>
        <BsCheck />
      </IconContext.Provider>
    );
  }

  return (
    <div className={checklistItemClass}>
      <div
        className={styles.Checkbox}
        tabIndex="0"
        onClick={updateTaskCompletionClickHandler}
        onKeyDown={updateTaskCompletionClickHandler}
      >
        {checkIcon}
      </div>
      {content}
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    boardId: state.board.boardData._id,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateChecklistItemName: (cardId, checklistItemName, token, boardId) => {
      dispatch(
        actions.updateChecklistItemName(
          cardId,
          checklistItemName,
          token,
          boardId
        )
      );
    },
    onUpdateTaskCompletion: (
      checklistItemId,
      completionState,
      token,
      boardId
    ) => {
      dispatch(
        actions.updateTaskCompletion(
          checklistItemId,
          completionState,
          token,
          boardId
        )
      );
    },
    onDeleteChecklistItem: (checklistItemId, token, boardId) => {
      dispatch(actions.deleteChecklistItem(checklistItemId, token, boardId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ChecklistItem);
