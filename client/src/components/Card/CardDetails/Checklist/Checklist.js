import React, { useState } from "react";
import { connect } from "react-redux";

import * as actions from "../../../../store/actions";
import ChecklistItem from "./ChecklistItem/ChecklistItem";
import styles from "./Checklist.module.css";
import { IconContext } from "react-icons";
import { BsListCheck } from "react-icons/bs";
import ProgressBar from "react-bootstrap/ProgressBar";
import EditField from "../EditField/EditField";

const Checklist = (props) => {
  const [taskEditing, setTaskEditing] = useState({
    isEditing: false,
    editingId: null,
  });
  const [addNewItem, setAddNewItem] = useState(false);
  const getTaskDonePercentage = () => {
    if (props.checklistContent.length === 0) {
      return 0;
    }
    const tasksDone = props.checklistContent.filter((obj) => obj.done).length;
    const allTasks = props.checklistContent.length;
    return Math.floor((tasksDone / allTasks) * 100);
  };

  const addItemClickHandler = () => {
    setAddNewItem(true);
    setTaskEditing({
      isEditing: false,
      editingId: null,
    });
  };

  const deleteChecklistClickHandler = (event) => {
    if (event.type === "click") {
      props.onDeleteChecklist(props.cardId, props.token, props.boardId);
    } else if (event.key === "Enter") {
      props.onDeleteChecklist(props.cardId, props.token, props.boardId);
    }
  };

  const cancelNewItem = () => {
    setAddNewItem(false);
  };

  // const percentage = getTaskDonePercentage();
  let checklistContent = null;
  if (props.checklistContent.length > 0) {
    checklistContent = props.checklistContent.map((obj, index) => {
      if (obj._id === taskEditing.editingId) {
        return (
          <ChecklistItem
            content={obj}
            editTask={setTaskEditing}
            addChecklistItem={setAddNewItem}
            key={index}
            isEditing
          />
        );
      } else {
        return (
          <ChecklistItem
            content={obj}
            editTask={setTaskEditing}
            addChecklistItem={setAddNewItem}
            key={index}
            isEditing={false}
          />
        );
      }
    });
  }

  let checklistAction = <button onClick={addItemClickHandler}>Add item</button>;
  // if (addNewItem) {
  //   checklistAction = (
  //     <EditChecklistItem type="add item" cancelAddItem={setAddNewItem} />
  //   );
  // }
  if (addNewItem) {
    checklistAction = (
      <EditField
        placeholder="Add an item"
        action="Save"
        cancel={cancelNewItem}
        confirm={props.onAddNewChecklistItem}
        margin="27"
        id={props.cardId}
      />
    );
  }

  return (
    <div className={styles.Checklist}>
      <div className={styles.ChecklistHeader}>
        <IconContext.Provider value={{ size: "25px" }}>
          <BsListCheck />
        </IconContext.Provider>
        <h2>{props.checklistName}</h2>
        <button onClick={deleteChecklistClickHandler}>Delete</button>
      </div>
      <div className={styles.ChecklistContent}>
        <div className={styles.ProgressBar}>
          <span>{`${getTaskDonePercentage()}%`}</span>
          <ProgressBar
            variant="success"
            now={getTaskDonePercentage()}
            label={getTaskDonePercentage() + "%"}
            srOnly
          />
        </div>
        {checklistContent}
        {checklistAction}
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    cardId: state.card.cardData._id,
    token: state.auth.token,
    boardId: state.board.boardData._id
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onAddNewChecklistItem: (cardId, checklistItemName, token, boardId) => {
      dispatch(actions.createChecklistItem(cardId, checklistItemName, token, boardId));
    },
    onDeleteChecklist: (cardId, token, boardId) => {
      dispatch(actions.deleteChecklist(cardId, token, boardId));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Checklist);
