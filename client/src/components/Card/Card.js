import React, { Component } from "react";
import { Draggable } from "react-beautiful-dnd";
// import TextareaAutosize from "react-textarea-autosize";

import styles from "./Card.module.css";
import { Link } from "react-router-dom";


class Card extends Component {
  state = {
    isEdited: false,
    titleValue: this.props.cardContent.title,
  };


  editCardClickHandler = (event) => {
    console.log("edit click is happenning");
    // no keydown event needed since keyboard navigation will open the details page and users can edit there
    this.setState({ isEdited: true });
    console.log(event.target.getBoundingClientRect());
  };

  editCardChangeHandler = (event) => {
    this.setState({ titleValue: event.target.value });
  };

  render() {


    return (
      <Draggable draggableId={this.props.id} index={this.props.i}>
        {(provided, snapshot) => {
          // console.log(snapshot)
          let classes = [styles.Card];
          if (snapshot.isDragging === true) {
            classes.push(styles.dragging);
          }
          return (
            <Link
              to={{
                pathname: `/card/${this.props.cardContent._id}`,
                state: { modal: true },
              }}
              className={classes.join(" ")}
              ref={provided.innerRef}
              {...provided.draggableProps}
              {...provided.dragHandleProps}
            >
              <p className={styles.CardText}>{this.props.cardContent.title}</p>
            </Link>
          );
        }}
      </Draggable>
    );
  }
}

export default Card;
