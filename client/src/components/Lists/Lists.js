import React, { useState } from "react";
import { connect } from "react-redux";
import { DragDropContext, Droppable } from "react-beautiful-dnd";

// import * as actions from "../../store/actions";
import List from "./List/List";
import NewList from "./List/NewList/NewList";
import styles from "./Lists.module.css";

const Lists = (props) => {
  const [newList, setNewList] = useState(false);

  let newListContent = null;
  const content = props.content;
  const lists = content.map((list, i) => {
    return (
      <Droppable droppableId={i.toString()} key={list._id}>
        {(provided, snapshot) => {
          return (
            <List
              id={list._id}
              listContent={list}
              i={i}
              boardId={props.boardId}
              listRef={provided.innerRef}
              placeholder={provided.placeholder}
            />
          );
        }}
      </Droppable>
    );
  });

  const addNewListHandler = (value, event) => {
    setNewList(value);
  };

  const addNewListKeyDownHandler = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      setNewList(true);
    }
  };

  const addListButton = (
    <div
      tabIndex="0"
      className={styles.NewList}
      onClick={addNewListHandler}
      onKeyDown={addNewListKeyDownHandler}
    >
      <div className={styles.AddList}>
        <span className={styles.Icon}>+</span>
        <span>Create a new List</span>
      </div>
    </div>
  );

  newListContent = (
    <NewList
      order={props.order}
      boardId={props.boardId}
      autoFocus
      addNewList={addNewListHandler}
    />
  );

  return (
    <React.Fragment>
      <DragDropContext onDragEnd={props.dragEnd}>{lists}</DragDropContext>
      {newList ? newListContent : addListButton}
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    order: state.board.boardData.boardContent.length,
    token: state.auth.token
  };
};

// const mapDispatchToProps = (dispatch) => {
//   return {
//     onAddNewList: (title, order, boardId, token) => dispatch(actions.createList(title, order, boardId, token)),
//   };
// };

export default connect(mapStateToProps)(Lists);
