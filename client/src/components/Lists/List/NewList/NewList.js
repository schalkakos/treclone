import React, { useState } from "react";
// import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { connect } from "react-redux";

import styles from "./NewList.module.css";
import TextareaAutosize from "react-textarea-autosize";
import * as actions from "../../../../store/actions/Board";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";

const NewList = (props) => {
  const [newListTitle, setNewListTitle] = useState("");

  const newListTitleHandler = (event) => {
      event.preventDefault()
      const newTitle = event.target.value;
      if (newTitle) {
        const order = props.order + 1;
        const boardId = props.boardId;
        props.onAddNewList(newTitle, order, boardId, props.token);
      }
      props.addNewList(false);
  };

  const newListTitleKeyDownHandler = (event) => {
      // console.log('change handler ran')
      // console.log(event.key)
    if (event.key === "Enter") {
        event.preventDefault();
        const newTitle = event.target.value;
        if(!newTitle){
            return
        }
        const order = props.order + 1;
        const boardId = props.boardId;
        
        props.onAddNewList(newTitle, order, boardId, props.token);
        setNewListTitle("");
        // props.addNewCard(false);

    }else if(event.key === "Escape") {
      setNewListTitle("");
      props.addNewList(false);
    }
  }
  

  const newListTitleChangeHandler = (event) => {
    setNewListTitle(event.target.value);
  }

  const cancelAddNewListHandler = (event) => {
    props.addNewList(false)
  }

  return (
    <div className={styles.Column}>
      <div className={styles.Container}>
        <div className={styles.ListHeader}>
          <TextareaAutosize
            className={styles.Title}
            value={newListTitle}
            onBlur={newListTitleHandler}
            onChange={newListTitleChangeHandler}
            onKeyDown={newListTitleKeyDownHandler}
            autoFocus={props.autoFocus}
          />
        </div>
        <div className={styles.Actions}>
        <button
          onClick={newListTitleHandler}
          className={styles.AddCard}
        >
          Add new list
        </button>
        <IconContext.Provider value={{size: '30px', className: styles.Close}} onClick={cancelAddNewListHandler}>
          <MdClose />
        </IconContext.Provider>
        </div>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    token: state.auth.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAddNewList: (title, order, boardId, token) =>
      dispatch(actions.createList(title, order, boardId, token)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewList);
