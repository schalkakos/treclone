import React, { Component } from "react";
import { connect } from "react-redux";
import TextareaAutosize from "react-textarea-autosize";
import _ from "lodash";

import Card from "../../Card/Card";
import styles from "./List.module.css";
import cardStyles from "../../Card/Card.module.css";
import { BsThreeDots } from "react-icons/bs";
import { IconContext } from "react-icons";
import * as actions from "../../../store/actions/Board";
import ListOption from "./ListOptions/ListOption";
import Options from "../../../hoc/options/Options";

class List extends Component {
  constructor(props) {
    super(props);
    this.textAreaRef = React.createRef();
    this.optionsRef = React.createRef();
    this.state = {
      addNewCard: false,
      cardTitleValue: "",
      listTitle: this.props.listContent.title,
      listOption: false,
      filterOptions: {
        nameAscending: { title: 1 },
        nameDescending: { title: -1 },
        dateAscending: { created: 1 },
        dateDescending: { created: -1 },
      },
    };
  }

  // cardOptionClickHandler = (event) => {
  //   let cardElement = event.target.closest("." + cardStyles.Card);
  //   const { top, left, width } = cardElement.getBoundingClientRect();
  //   const cardCss = {
  //     position: "absolute",
  //     top: top,
  //     left: left,
  //     width: width,
  //     boxSizing: "border-box",
  //   };
  //   this.setState({
  //     editCard: true,
  //     editCardValue: cardElement.firstChild.innerHTML,
  //     editCardCss: cardCss,
  //   });
  // };

  cardChangeHandler = (event) => {
    this.setState({ editCardValue: event.target.value });
  };

  editListTitleHandler = (event) => {
    if (event.key === "Enter") {
      event.preventDefault();
      this.textAreaRef.current.blur();
    }
    if (event.type === "blur") {
      const updatedTitle = event.target.value;
      if (
        updatedTitle !== this.props.listContent.title &&
        updatedTitle !== ""
      ) {
        const _id = this.props.listContent._id;
        this.props.onUpdateListTitle(_id, updatedTitle, this.props.token, this.props.boardId);
      } else if (updatedTitle === "") {
        this.setState({ listTitle: this.props.listContent.title });
        // setListTitle(props.listContent.title);
      }
    }
  };

  updateListSortAction = (event) => {
    const _id = this.props.listContent._id;
    const sortBy = this.state.filterOptions[event.target.value];

    if (!_.isEqual(sortBy, this.props.listContent.sortBy))
      this.props.onUpdateListSort(_id, sortBy, this.props.token, this.props.boardId);
  };

  updateListSortHandler = (event) => {
    if (event.type === "click") {
      this.updateListSortAction(event);
      this.setState({ listOption: false });
    } else if (event.key === "Enter") {
      event.preventDefault();
      this.updateListSortAction(event);
      this.setState({ listOption: false });
    }
  };

  listTitleChangeHandler = (event) => {
    this.setState({ listTitle: event.target.value });
    // setListTitle(event.target.value);
  };

  newCardHandler = (event) => {
    const title = event.target.value;
    const order = this.props.listContent.cards.length + 1;

    if (event.type === "keydown") {
      if (event.key === "Escape") {
        this.setState({ addNewCard: false, cardTitleValue: "" });
      } else if (event.key === "Enter") {
        event.preventDefault();
        if (title.length > 0) {
          this.setState({ cardTitleValue: "" });
          this.props.onAddCard(title, this.props.listContent._id, order, this.props.token, this.props.boardId);
        } else {
          this.setState({ addNewCard: false, cardTitleValue: "" });
        }
      }
    } else if (event.type === "blur") {
      if (title.length > 0) {
        this.props.onAddCard(title, this.props.listContent._id, order, this.props.token, this.props.boardId);
        this.setState({ addNewCard: false, cardTitleValue: "" });
      } else {
        this.setState({ addNewCard: false });
      }
    }
  };

  newCardChangeHandler = (event) => {
    this.setState({ cardTitleValue: event.target.value });
    // setCardTitleValue(event.target.value);
  };

  addNewCardHandler = () => {
    this.setState({ addNewCard: true });
  };

  listOptionHandler = (event) => {
    if (!this.state.listOption) {
      if (event.type === "click") {
        this.setState({ listOption: true });
      } else if (event.type === "keydown") {
        if (event.key === "Enter") {
          event.preventDefault();
          this.setState({ listOption: true });
        }
      }
    } else {
      if (event.type === "click") {
        this.setState({ listOption: false });
      } else if (event.type === "keydown") {
        if (event.key === "Enter") {
          event.preventDefault();
          this.setState({ listOption: false });
        }
      }
    }
  };

  deleteListHandler = (event) => {
    if (event.type === "click") {
      this.props.onDeleteList(this.props.listContent._id, this.props.token, this.props.boardId);
    } else if (event.type === "keydown") {
      if (event.key === "Enter") {
        event.preventDefault();
        this.props.onDeleteList(this.props.listContent._id, this.props.token, this.props.boardId);
      }
    }
  };

  // backdropClickHandler = () => {
  //   this.setState({ editCard: false });
  // };

  setListOption = (value) => {
    this.setState({ listOption: value });
  };

  render() {
    // let editCard = null;
    // if (this.state.editCard) {
    //   editCard = (
    //     <EditCard
    //       editCardCss={this.state.editCardCss}
    //       editCardValue={this.state.editCardValue}
    //       cardChange={this.state.cardChangeHandler}
    //       optionsRef={this.optionsRef}
    //       backdropClick={this.backdropClickHandler}
    //     />
    //   );
    // }

    const listContent = this.props.listContent;
    // console.log(props.listContent);
    const cards = listContent.cards.map((card, i) => {
      // console.log(card)
      return (
        <Card
          optionClick={this.cardOptionClickHandler}
          key={card._id}
          id={card._id}
          cardContent={card}
          i={i}
        />
      );
    });

    let newCardField = null;
    if (this.state.addNewCard === true) {
      const classes = [cardStyles.Card, styles.NewCard];
      newCardField = (
        <div tabIndex="0" className={styles.NewCardWrapper}>
          <textarea
            value={this.state.cardTitleValue}
            onChange={this.newCardChangeHandler}
            className={classes.join(" ")}
            onBlur={this.newCardHandler}
            onKeyDown={this.newCardHandler}
            placeholder="Enter a title for the new card"
            autoFocus
          />
        </div>
      );
    }

    let options = null;

    if (this.state.listOption) {
      options = (
        <Options
          optionsRef={this.optionsRef}
          showOptions={this.setListOption}
          scroll
        >
          <ListOption
            optionsRef={this.optionsRef}
            deleteList={this.deleteListHandler}
            addCard={this.addNewCardHandler}
            showOptions={this.setListOption}
            editListOrder={this.updateListSortHandler}
            listId={this.props.listContent._id}
          />
        </Options>
      );
    }
    return (
      <div className={styles.Column} ref={this.props.listRef}>
        {/* <Droppable droppableId={this.props.listContent._id}> */}
        {/* {(provided, snapshot) => {
            return ( */}
        <div
          className={styles.Container}
          // ref={provided.innerRef}
          // {...provided.droppableProps}
        >
          <div className={styles.ListHeader}>
            {/* <p>{listContent.title}</p> */}
            <TextareaAutosize
              className={styles.Title}
              value={this.state.listTitle}
              onChange={this.listTitleChangeHandler}
              onBlur={this.editListTitleHandler}
              onKeyDown={this.editListTitleHandler}
              ref={this.textAreaRef}
              autoFocus={this.props.autoFocus}
            />
            <div
              tabIndex="0"
              onClick={this.listOptionHandler}
              onKeyDown={this.listOptionHandler}
              className={styles.Option}
            >
              <IconContext.Provider value={{ size: "25px" }}>
                <BsThreeDots />
              </IconContext.Provider>
            </div>
            {options}
          </div>
          <div className={styles.CardList}>
            {cards}
            {this.props.placeholder}
            {newCardField}
          </div>

          <button onClick={this.addNewCardHandler} className={styles.AddCard}>
            Add new card
          </button>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    token: state.auth.token,
    boardId: state.board.boardData._id
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onUpdateListTitle: (id, updatedTitle, token, boardId) =>
      dispatch(actions.updateListTitle(id, updatedTitle, token, boardId)),
    onUpdateListOrder: (id, updatedOrder, token, boardId) =>
      dispatch(actions.updateListOrder(id, updatedOrder, token, boardId)),
    onUpdateListSort: (id, updatedSort, token, boardId) =>
      dispatch(actions.updateListSort(id, updatedSort, token, boardId)),
    onAddCard: (title, listId, order, token, boardId) => {
      dispatch(actions.addCard(title, listId, order, token, boardId));
    },
    onDeleteList: (listId, token, boardId) => dispatch(actions.deleteList(listId, token, boardId)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
