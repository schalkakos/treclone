import React, { useState, useRef } from "react";

import ListActions from "./ListOptionsContent/ListActions";
import ListMoveOptions from "./ListOptionsContent/ListMoveOptions";
import ListSortOptions from "./ListOptionsContent/ListSortOptions";

const ListOption = (props) => {
  const [optionsContent, setOptionsContent] = useState("");

  const sortbyRef = useRef(null);

  const sortByHandler = (event) => {
    if (event.type === "click") {
      setOptionsContent("sort options");
      sortbyRef.current.blur();
    } else {
      if (event.key === "Enter") {
        event.preventDefault();
        setOptionsContent("sort options");
        sortbyRef.current.blur();
      }
    }
  };

  const moveButtonClickHandler = (event) => {
    if (event.type === "click") {
      setOptionsContent("move");
      // sortbyRef.current.blur();
    } else {
      if (event.key === "Enter") {
        event.preventDefault();
        setOptionsContent("move");
        // sortbyRef.current.blur();
      }
    }
  };

  const backIconClickHandler = (event) => {
    if (event.type === "click") {
      setOptionsContent("");
    } else {
      if (event.key === "Enter") {
        setOptionsContent("");
      }
    }
  };

  const closeOptionsHandler = (event) => {
    if (event.type === "click") {
      // console.log("close options");
      props.showOptions(false);
    } else {
      if (event.key === "Enter") {
        // console.log("close options");
        event.preventDefault();
        props.showOptions(false);
      }
    }
  };

  const addNewCardHandler = (event) => {
    if (event.type === "click") {
      // console.log("close options");
      props.addCard();
      props.showOptions(false);
    } else {
      if (event.key === "Enter") {
        event.preventDefault();
        props.addCard();
        props.showOptions(false);
      }
    }
  };

  let content = null;

  switch (optionsContent) {
    case "sort options":
      content = (
        <ListSortOptions
          backIconClickHandler={backIconClickHandler}
          closeOptionsHandler={closeOptionsHandler}
          editListOrder={props.editListOrder}
        />
      );
      break;
    case "move":
      content = (
        <ListMoveOptions
          backIconClick={backIconClickHandler}
          closeOptions={closeOptionsHandler}
          listId={props.listId}
        />
      );
      break;
    default:
      content = (
        <ListActions
          sortBy={sortByHandler}
          sortby={sortbyRef}
          moveClick={moveButtonClickHandler}
          closeOptions={closeOptionsHandler}
          addNewCard={addNewCardHandler}
          deleteList={props.deleteList}
        />
      );
  }

  return content;
};

export default ListOption;
