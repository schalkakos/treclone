import React, { useState } from "react";
import { connect } from "react-redux";

import styles from "../ListOption.module.css";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";
import { RiArrowLeftSLine } from "react-icons/ri";
import * as actions from "../../../../../store/actions";
import { makeStyles } from "@material-ui/core/styles";
import { getListIndex } from "../../../../../store/utils";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

// //material ui css styles
const useStyles = makeStyles((theme) => ({
  listFormControl: {
    margin: theme.spacing(1),
    width: 180,
  },
  cardFormControl: {
    margin: theme.spacing(1),
  },
  input: {
    "& .MuiOutlinedInput-input": {
      padding: "12px 32px 12px 12px",
    },
  },
}));

const ListMoveOptions = (props) => {
  const {
    closeOptions,
    backIconClick,
    lists,
    listId,
    onMoveList,
    boardId,
    token
  } = props;
  const indexes = getListIndex(lists, listId);
  const [originalListIndex] = useState(indexes.listIndex);
  const [currentListIndex, setCurrentListIndex] = useState(indexes.listIndex);
  const classes = useStyles();

  const listSelectChangeHandler = (event) => {
    setCurrentListIndex(event.target.value);
  };

  let listItems = lists.map((obj, index) => {
    //if the map object is the original list indicate that that's the current one else just display the list position
    if (indexes.listIndex === index) {
      return (
        <MenuItem key={index} value={index}>
          {`${index + 1} (current)`}
        </MenuItem>
      );
    } else {
      return (
        <MenuItem key={index} value={index}>
          {index + 1}
        </MenuItem>
      );
    }
  });

  const confirmHandler = (event) => {
    const oldListPos = originalListIndex;
    const newListPos = currentListIndex;
    // console.log(listId, boardId)
    if (event.type === "click" && oldListPos !== newListPos) {
      //do smg
      onMoveList(oldListPos, newListPos, listId, boardId, token);
    } else if (event.key === "Enter" && oldListPos !== newListPos) {
      //do the same smg
      onMoveList(oldListPos, newListPos, listId, boardId, token);
    }
    closeOptions(event);
  };

  return (
    <div className={styles.Option}>
      <div className={styles.OptionHeaderSort}>
        <div
          tabIndex="0"
          onClick={backIconClick}
          onKeyDown={backIconClick}
          className={styles.Back}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <RiArrowLeftSLine />
          </IconContext.Provider>
        </div>
        <span>Move List</span>
        <div
          tabIndex="0"
          onClick={closeOptions}
          onKeyDown={closeOptions}
          className={styles.Close}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <MdClose />
          </IconContext.Provider>
        </div>
      </div>
      <FormControl variant="outlined" className={classes.listFormControl}>
        <InputLabel id="list">List</InputLabel>
        <Select
          className={classes.input}
          label="List"
          //renderValue is needed so input doesn't display the current text just the value as it's only needed in the dropdown
          renderValue={() => currentListIndex + 1}
          value={currentListIndex}
          onChange={listSelectChangeHandler}
        >
          {listItems}
        </Select>
      </FormControl>
      <div className={styles.Actions}>
        <button
          className={styles.SuccessButton}
          onClick={confirmHandler}
          onKeyDown={confirmHandler}
        >
          Move
        </button>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    lists: state.board.boardData.boardContent,
    boardId: state.board.boardData._id,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onMoveList: (oldListPos, newListPos, listId, boardId, token) =>
      dispatch(
        actions.moveList(oldListPos, newListPos, listId, boardId, token)
      ),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ListMoveOptions);
