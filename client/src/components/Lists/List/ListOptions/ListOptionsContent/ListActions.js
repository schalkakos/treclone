import React from "react";

import styles from "../ListOption.module.css";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";

const ListActions = (props) => {
  return (
    <div className={styles.Option}>
      <div className={styles.OptionHeader}>
        <span>List Actions</span>
        <div
          tabIndex="0"
          onClick={props.closeOptions}
          onKeyDown={props.closeOptions}
          className={styles.Close}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <MdClose />
          </IconContext.Provider>
        </div>
      </div>
      <li>
        <button
          value="New Card"
          onClick={props.addNewCard}
          onKeyDown={props.addNewCard}
        >
          New card
        </button>
      </li>
      <li>
        <button
          onClick={props.moveClick}
          onKeyDown={props.moveClick}
          ref={props.sortby}
        >
          Move
        </button>
      </li>
      <li>
        <button
          onClick={props.sortBy}
          onKeyDown={props.sortBy}
          ref={props.sortby}
        >
          Sort by:
        </button>
      </li>
      <hr />
      <li>
        <button onClick={props.deleteList} onKeyDown={props.deleteList}>
          Delete list
        </button>
      </li>
    </div>
  );
};

export default ListActions;
