import React from "react";

import styles from "../ListOption.module.css";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";
import { RiArrowLeftSLine } from "react-icons/ri";

const ListSortOptions = (props) => {
  return (
    <div className={styles.Option}>
      <div className={styles.OptionHeaderSort}>
        <div
          tabIndex="0"
          onClick={props.backIconClickHandler}
          onKeyDown={props.backIconClickHandler}
          className={styles.Back}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <RiArrowLeftSLine />
          </IconContext.Provider>
        </div>
        <span>Sort By</span>
        <div
          tabIndex="0"
          onClick={props.closeOptionsHandler}
          onKeyDown={props.closeOptionsHandler}
          className={styles.Close}
        >
          <IconContext.Provider value={{ size: "25px" }}>
            <MdClose />
          </IconContext.Provider>
        </div>
      </div>
      <li>
        <button
          value="dateAscending"
          onClick={props.editListOrder}
          onKeyDown={props.editListOrder}
        >
          Date [0-9]
        </button>
      </li>
      <li>
        <button
          value="dateDescending"
          onClick={props.editListOrder}
          onKeyDown={props.editListOrder}
        >
          Date [9-0]
        </button>
      </li>
      <li>
        <button
          value="nameAscending"
          onClick={props.editListOrder}
          onKeyDown={props.editListOrder}
        >
          Alphabetically [a-z]
        </button>
      </li>
      <li>
        <button
          value="nameDescending"
          onClick={props.editListOrder}
          onKeyDown={props.editListOrder}
        >
          Alphabetically [z-a]
        </button>
      </li>
    </div>
  );
};

export default ListSortOptions;
