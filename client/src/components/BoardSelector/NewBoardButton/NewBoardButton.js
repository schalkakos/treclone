import React from "react";

import styles from "./NewBoardButton.module.css";

const NewBoardButton = (props) => {
  const { newBoard } = props;
  return (
    <div tabIndex="0" className={styles.NewBoard} onClick={() => newBoard(true)}>
      <div className={styles.AddBoard}>
        <span>Create board</span>
      </div>
    </div>
  );
};

export default NewBoardButton;

