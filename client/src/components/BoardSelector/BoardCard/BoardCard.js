import React from "react";
import { Link } from "react-router-dom";

import styles from "./BoardCard.module.css";

const BoardCard = (props) => {
  const { title, boardId } = props;

  return (
    <Link className={styles.CardWrapper} to={`/board/${boardId}`}>
      <div>
        <h3 className={styles.Title}>{title}</h3>
      </div>
    </Link>
  );
};

export default BoardCard;
