import React, { useState } from "react";
import { connect } from "react-redux";

import styles from "./NewBoardField.module.css";
import * as actions from "../../../store/actions";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";

const NewBoardField = (props) => {
  const [newBoardTitle, setNewBoardTitle] = useState("");
  const { onCreateBoard, userId, newBoard, token} = props;

  const boardTitleChangeHandler = (event) => {
    setNewBoardTitle(event.target.value);
  }

  const boardTitleSubmitHandler = (event) => {
    if (event.type === "click") {
      newBoard(false);
      onCreateBoard(newBoardTitle, userId, token);
    } else if (event.type === "keydown" && event.key === "Enter") {
      event.preventDefault();
      newBoard(false);
      onCreateBoard(newBoardTitle, userId, token);
    }
  };

  return (
    <div className={styles.CardWrapper}>
      <div className={styles.ListHeader}>
        <input
          className={styles.Title}
          value={newBoardTitle}
          onChange={boardTitleChangeHandler}
          onKeyDown={boardTitleChangeHandler}
          autoFocus={props.autoFocus}
        />
      </div>
      <div className={styles.Actions}>
        <button
          onClick={boardTitleSubmitHandler}
          className={styles.AddCard}
        >
          Create board
        </button>
        <IconContext.Provider
          value={{ size: "30px", className: styles.Close }}
          // onClick={cancelAddNewListHandler}
        >
          <MdClose />
        </IconContext.Provider>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    userId: state.auth.userId,
    token: state.auth.token
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onCreateBoard: (boardTitle, userId, token) => dispatch(actions.createBoard(boardTitle, userId, token))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(NewBoardField);