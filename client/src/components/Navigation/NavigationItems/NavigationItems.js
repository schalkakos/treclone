import React from 'react';
import { connect } from "react-redux"
import styles from './NavigationItems.module.css'
import NavigationItem from './NavigationItem/NavigationItem';


const navigationItem = (props) => {
    const { userId } = props;
    return (
        <div className={styles.NavigationItems}>
            <NavigationItem link={`/${userId}/boards`}>Home</NavigationItem>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        userId: state.auth.userId
    }
}

export default connect(mapStateToProps)(navigationItem);