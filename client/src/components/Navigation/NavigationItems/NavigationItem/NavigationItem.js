import React from 'react';

import styles from './NavigationItem.module.css';


const navigationItem = (props) => {

    return (
        <a href={props.link} className={styles.NavigationItem}>
            <p>{props.children}</p>
        </a>
    );
}

export default navigationItem;