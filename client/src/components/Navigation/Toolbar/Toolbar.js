import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/'; 

import NavigationItems from '../NavigationItems/NavigationItems';
import styles from './Toolbar.module.css';


const Toolbar = (props) => {
    const { onLogout } = props

    return (
        <nav className={styles.Toolbar}>
        <NavigationItems />

        <div className={styles.Logout} onClick={onLogout}>
            <p>Logout</p>
        </div>
        </nav>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        onLogout: () => {dispatch(actions.logout())}
    }
}

export default connect(null, mapDispatchToProps)(Toolbar);