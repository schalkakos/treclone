import React, { useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import { Formik, Field } from "formik";
import { connect } from "react-redux";
import * as actions from "../../store/actions";
import { useHistory } from "react-router-dom";
import {  } from "react-router-dom";

import styles from "./Signup.module.css";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",
    },
    width: "320px",
  },
  input: {
    "& .MuiOutlinedInput-input": {
      width: "300px",
    },
  },
  error: {
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "#f44336",
    },
    "& .MuiFormLabel-root": {
      color: "#f44336",
    },
  },
}));

const validate = (values) => {
  const errors = {};

  if (!values.email) {
    errors.email = <p className={styles.ValidationError}>Required</p>;
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = (
      <p className={styles.ValidationError}>Invalid email address</p>
    );
  }

  if (!values.password) {
    errors.password = <p className={styles.ValidationError}>Required</p>;
  } else if (values.password.length < 8) {
    errors.password = (
      <p className={styles.ValidationError}>Must be 8 characters or more</p>
    );
  }

  if (!values.confirm) {
    errors.confirm = <p className={styles.ValidationError}>Required</p>;
  } else if (values.password !== values.confirm) {
    errors.confirm = (
      <p className={styles.ValidationError}>Must match the password</p>
    );
  } else if (values.confirm.length < 8) {
    errors.confirm = (
      <p className={styles.ValidationError}>Must be 8 characters or more</p>
    );
  }
  return errors;
};

const Signup = (props) => {
  const classes = useStyles();
  const { onSignup, error, isLoggedIn } = props;
  const history = useHistory();
  useEffect(() => {
    if (error === false && !isLoggedIn) {
      history.push("/login");
    } else if (isLoggedIn) {
      history.push("/");
    }
  }, [error, history, isLoggedIn]);

  let emailClasses = classes.input;
  let passwordClasses = classes.input;
  let confirmPasswordClasses = classes.input;
  if (error) {
    if (error.field === "email") {
      emailClasses = [classes.input, classes.error].join(" ");
    } else if (error.field === "password") {
      passwordClasses = [classes.input, classes.error].join(" ");
    } else {
      confirmPasswordClasses = [classes.input, classes.error].join(" ");
    }
  }

  const formSubmitHandler = (values) => {
    const { email, password, confirm } = values;
    // console.log(values)

    onSignup(email, password, confirm);
  };

  return (
    <main className={styles.Signup}>
      <div className={styles.SignupFormWrapper}>
        <h3>Create a TreClone account</h3>
        {error && <span className={styles.Error}>{error.message}</span>}
        <Formik
          initialValues={{ email: "", password: "", confirm: "" }}
          validate={validate}
          onSubmit={(values, { setSubmitting }) => {
            formSubmitHandler(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit} className={classes.root}>
              <Field
                component={TextField}
                className={emailClasses}
                id="email"
                name="email"
                label="Email"
                // helperText="Required *"
                required
                variant="outlined"
                type="email"
                size="small"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              {errors.email && touched.email && errors.email}
              <Field
                component={TextField}
                className={passwordClasses}
                id="password"
                name="password"
                label="Password"
                // helperText="Required *"
                required
                variant="outlined"
                type="password"
                size="small"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              {errors.password && touched.password && errors.password}
              <Field
                component={TextField}
                className={confirmPasswordClasses}
                id="confirm"
                name="confirm"
                label="Confirm"
                // helperText="Required *"
                required
                variant="outlined"
                type="password"
                size="small"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.confirm}
              />
              {errors.confirm && touched.confirm && errors.confirm}
              <div className={styles.FormActions}>
                <button
                  disabled={isSubmitting}
                  type="submit"
                  className={styles.SignupButton}
                >
                  Sign Up
                </button>
                <Link className={styles.Register} to="/login">
                  Sign in
                </Link>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </main>
  );
};

const mapStateToProps = (state) => {
  return {
    error: state.auth.error,
    isLoggedIn: state.auth.token ? true : false,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSignup: (email, password, confirmPassword) => {
      dispatch(actions.signup(email, password, confirmPassword));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
