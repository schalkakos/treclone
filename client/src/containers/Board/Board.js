import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import { connect } from "react-redux";

import CardDetails from "../CardDetails/CardDetails";
import styles from "./Board.module.css";
import Lists from "../../components/Lists/Lists";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import * as actions from "../../store/actions/index";


class Board extends Component {

  componentDidMount() {
    if (!this.props.content._id && this.props.match.params.boardId) {
      this.props.onGetBoard(this.props.match.params.boardId, this.props.token, this.props.history);
    } else {
      this.props.onGetBoardFromLocalStorage(this.props.token)
    }
  }

  dragEndHandler = (dragEvent) => {
    //source is the original card and list position
    //destination is the new card list position
    const { source, destination } = dragEvent;
    const cardId = this.props.content.boardContent[parseInt(source.droppableId)]
      .cards[source.index]._id;
    const oldListId = this.props.content.boardContent[
      parseInt(source.droppableId)
    ]._id;


    const oldListPos = parseInt(source.droppableId);
    const oldCardPos = source.index;
    const newListPos = parseInt(destination.droppableId);
    const newCardPos = destination.index;
    if (oldListPos === newListPos) {
      this.props.onMoveCardSameList(
        oldListPos,
        oldCardPos,
        newListPos,
        newCardPos,
        cardId,
        oldListId,
        this.props.token,
        this.props.content._id
      );
    } else {
      const newlistId = this.props.content.boardContent[
        parseInt(destination.droppableId)
      ]._id;
      this.props.onMoveCardDifferentList(
        oldListPos,
        oldCardPos,
        newListPos,
        newCardPos,
        cardId,
        oldListId,
        newlistId,
        this.props.token,
        this.props.content._id
      );
    }
  };

  render() {


    return (
      <React.Fragment>
        <Toolbar />
        <main className={styles.Main}>
          <div className={styles.ListsWrapper}>
            <Lists
              content={this.props.content.boardContent}
              dragEnd={this.dragEndHandler}
              boardId={this.props.content._id}
            />
          </div>
        </main>
        {this.props.modal ? <CardDetails /> : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userId: state.auth.userId,
    content: state.board.boardData,
    token: state.auth.token,
    error: state.board.error,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetBoard: (boardId, token, history) => dispatch(actions.getBoard(boardId, token, history)),
    onMoveCardDifferentList: (
      oldListPos,
      oldCardPos,
      newListPos,
      newCardPos,
      cardId,
      oldListId,
      newListId,
      token,
      boardId
    ) => {
      dispatch(
        actions.moveCardDifferentList(
          oldListPos,
          oldCardPos,
          newListPos,
          newCardPos,
          cardId,
          oldListId,
          newListId,
          token,
          boardId
        )
      );
    },
    onMoveCardSameList: (
      oldListPos,
      oldCardPos,
      newListPos,
      newCardPos,
      cardId,
      listId,
      token,
      boardId
    ) =>
      dispatch(
        actions.moveCardSameList(
          oldListPos,
          oldCardPos,
          newListPos,
          newCardPos,
          cardId,
          listId,
          token,
          boardId
        )
      ),
    onGetBoardFromLocalStorage: (token) => {
      dispatch(actions.getBoardIdFromLocalStorage(token))
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Board));
