import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { useParams } from "react-router-dom";

import * as actions from "../../store/actions";
import Toolbar from "../../components/Navigation/Toolbar/Toolbar";
import BoardCard from "../../components/BoardSelector/BoardCard/BoardCard";
import styles from "./BoardSelector.module.css";
import NewBoardButton from "../../components/BoardSelector/NewBoardButton/NewBoardButton";
import NewBoardField from "../../components/BoardSelector/NewBoardField/NewBoardField";

const BoardSelector = (props) => {
  const { onGetBoards, boards, loading, token, history } = props;
  const { userId } = useParams();
  const [newBoard, setNewBoard] = useState(false);
  // const newBoardHandler = () => {
  //   setNewBoard(true);
  // }

  useEffect(() => {
    onGetBoards(userId, token, history);
  }, [onGetBoards, userId, token, history]);

  // console.log(loading)
  // console.log(boards)
  let content;
  if (loading === false) {
    if (boards) {
      content = boards.map((boardData, index) => {
        return (
          <BoardCard
            title={boardData.name}
            boardId={boardData._id}
            key={index}
          />
        );
      });
    }
  }

  return (
    <React.Fragment>
      <Toolbar />
      <main className={styles.Main}>
        {content}
        {newBoard ? (
          <NewBoardField newBoard={setNewBoard} />
        ) : (
          <NewBoardButton newBoard={setNewBoard} />
        )}
      </main>
    </React.Fragment>
  );
};

const mapStateToProps = (state) => {
  return {
    boards: state.boardSelector.boards,
    loading: state.boardSelector.loading,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetBoards: (userId, token, history) => dispatch(actions.getBoards(userId, token, history)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BoardSelector);
