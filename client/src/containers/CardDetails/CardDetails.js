import React, { useEffect, useState, useRef, useCallback } from "react";
// import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { withRouter, useParams } from "react-router-dom";

import Spinner from "react-bootstrap/Spinner";
import CardTitle from "../../components/Card/CardDetails/CardTitle/CardTitle";
import Description from "../../components/Card/CardDetails/Description/Description";
import * as actions from "../../store/actions";
import styles from "./CardDetails.module.css";
import Backdrop from "../../hoc/Backdrop/Backdrop";
import { MdClose } from "react-icons/md";
import { IconContext } from "react-icons";

import CardActions from "../../components/Card/CardDetails/CardActions/CardActions";
import Checklist from "../../components/Card/CardDetails/Checklist/Checklist";
import DueDate from "../../components/Card/CardDetails/DueDate/DueDate";

const CardDetails = (props) => {
  const [cardFitsOnScreen, setCardFitsOnScreen] = useState(true);
  const cardDetailsRef = useRef(null);

  const { cardId } = useParams();
  const { onGetCard, fetching, token, boardId } = props;

  useEffect(() => {
    if (props.cardContent._id === cardId) {
      return;
    }
    onGetCard(cardId, token, boardId);
  }, [onGetCard, cardId, props.cardContent._id, token, boardId]);

  const modalFitsScreen = useCallback(() => {
    if (fetching === false) {
      const cardDetailsHtml = document.querySelector(
        "div." + styles.CardDetailWrapper
      );
      const { bottom } = cardDetailsHtml.getBoundingClientRect();
      const windowHeight = window.innerHeight;

      if (bottom > windowHeight) {
        setCardFitsOnScreen(false);
      } else {
        setCardFitsOnScreen(true);
      }
    }
  }, [fetching]);

  const dueDateCheckboxClickHandler = () => {
    console.log(token);
    props.onUpdateDueDateDone(
      props.cardContent._id,
      props.cardContent.listId,
      !props.cardContent.dueDateDone,
      props.token,
      props.boardId
    );
  };

  const updateDueDateChangeHandler = (updatedDueDate) => {
    console.log(token);
    props.onUpdateDueDate(
      props.cardContent._id,
      props.cardContent.listId,
      updatedDueDate,
      props.token,
      props.boardId
    );
  };

  useEffect(() => {
    window.addEventListener("resize", modalFitsScreen);
    return () => {
      window.removeEventListener("resize", modalFitsScreen);
    };
  });

  useEffect(() => {
    modalFitsScreen();
  }, [props.fetching, modalFitsScreen]);

  let cardDetailWrapperStyle = {};
  if (cardFitsOnScreen === false) {
    cardDetailWrapperStyle = {
      top: "5px",
      alignSelf: "flex-start",
    };
  }

  let content = (
    <div className={styles.CardDetailsModal}>
      <Backdrop />
      <div
        ref={cardDetailsRef}
        className={styles.CardDetailWrapper}
        style={cardDetailWrapperStyle}
      >
        <Spinner animation="border" />
      </div>
    </div>
  );

  if (props.fetching === false) {
    content = (
      <div className={styles.CardDetailsModal}>
        <Backdrop />
        <div
          ref={cardDetailsRef}
          className={styles.CardDetailWrapper}
          style={cardDetailWrapperStyle}
        >
          <div className={styles.CardContent}>
            <CardTitle
              cardTitle={props.cardContent.title}
              updateTitle={props.onUpdateTitle}
              token={props.token}
              boardId={props.boardId}
              cardId={props.cardContent._id}
              listId={props.cardContent.listId}
            />
            <Description
              description={props.cardContent.description}
              id={props.cardContent._id}
            />
            {props.cardContent.dueDate ? (
              <DueDate
                dueDate={props.cardContent.dueDate}
                dueDateDone={props.cardContent.dueDateDone}
                markAsDone={dueDateCheckboxClickHandler}
                changeDate={updateDueDateChangeHandler}
              />
            ) : null}
            {props.cardContent.checklist ? (
              <Checklist
                checklistName={props.cardContent.checklistName}
                checklistContent={props.cardContent.checklistItems}
              />
            ) : null}
          </div>
          <CardActions />
          <Link to={`/board/${props.boardId}`}>
            <button className={styles.Close}>
              <IconContext.Provider value={{ size: "20px" }}>
                <MdClose />
              </IconContext.Provider>
            </button>
          </Link>
        </div>
      </div>
    );
  }

  return content;
};

const mapStateToProps = (state) => {
  return {
    fetching: state.card.isFetching,
    boardId: state.board.boardData._id,
    cardContent: state.card.cardData,
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onGetCard: (cardId, token, boardId) => dispatch(actions.getCard(cardId, token, boardId)),
    onUpdateTitle: (cardId, listId, updatedTitle, token, boardId) =>
      dispatch(actions.updateCardTitle(cardId, listId, updatedTitle, token, boardId)),
    onUpdateDueDate: (cardId, listId, updatedDueDate, token, boardId) =>
      dispatch(actions.updateDueDate(cardId, listId, updatedDueDate, token, boardId)),
    onUpdateDueDateDone: (cardId, listId, updatedDueDateDone, token, boardId) =>
      dispatch(
        actions.updateDueDateDone(cardId, listId, updatedDueDateDone, token, boardId)
      ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CardDetails));
