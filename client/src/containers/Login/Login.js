import React, { useEffect } from "react";
import TextField from "@material-ui/core/TextField";
import { Link } from "react-router-dom";
import { Formik, Field } from "formik";
import { connect } from "react-redux";
import * as actions from "../../store/actions";
import { useHistory } from "react-router-dom";

import styles from "./Login.module.css";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "300px",

    },
    width: "320px",
  },
  input: {
    "& .MuiOutlinedInput-input": {
      width: "300px",
      borderColor: "#f44336"
    },
  },
  error: {
    "& .MuiOutlinedInput-notchedOutline": {
      borderColor: "#f44336"
    },
    "& .MuiFormLabel-root": {
      color: "#f44336"
    }

  }
}));

const Login = (props) => {
  const classes = useStyles();
  const { error, onLogin, isLoggedIn, userId, redirectPath } = props;
  const history = useHistory();


  useEffect(() => {
    if(isLoggedIn) {
      if(redirectPath !== "/") {
        history.push(redirectPath);
        return;
      }
      history.push(`/${userId}/boards`);
    }
  }, [isLoggedIn, userId, history, redirectPath]);

  let emailClasses = classes.input;
  let passwordClasses = classes.input;
  if(error){
    if(error.field === "email") {
      emailClasses = [classes.input, classes.error].join(" ")

    }else {
      passwordClasses = [classes.input, classes.error].join(" ")
    }
  }

  const formSubmitHandler = (values) => {
    const { email, password } = values;

    onLogin(email, password);
  };

  return (
    <main className={styles.Login}>
      <div className={styles.LoginFormWrapper}>
        <h3>Login to TreClone</h3>
        {error ? (<span className={styles.Error}>{error.message}</span>) : null}
        <Formik
          initialValues={{ email: "", password: "" }}
          onSubmit={(values, { setSubmitting }) => {
            formSubmitHandler(values);
            setSubmitting(false);
          }}
        >
          {({
            values,
            handleChange,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit} noValidate className={classes.root}>
              <Field
                component={TextField}
                className={emailClasses}
                id="email"
                label="Email"
                // helperText="Required *"
                required
                variant="outlined"
                type="email"
                size="small"
                onChange={handleChange}
                value={values.email}
              />
              <Field
                component={TextField}
                className={passwordClasses}
                id="password"
                label="Password"
                // helperText="Required *"
                required
                variant="outlined"
                type="password"
                size="small"
                onChange={handleChange}
                values={values.password}
              />
              <div className={styles.FormActions}>
                <button
                  type="submit"
                  disabled={isSubmitting}
                  className={styles.LoginButton}
                >
                  Login
                </button>
                <Link className={styles.Register} to="/signup">
                  Create an account
                </Link>
              </div>
            </form>
          )}
        </Formik>
      </div>
    </main>
  );
};

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.token ? true : false,
    userId: state.auth.userId,
    redirectPath: state.auth.path,
    error: state.auth.error
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onLogin: (email, password) => {
      dispatch(actions.signinStart(email, password));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

// import React from 'react';

// export default function FormPropsTextFields() {
//   const classes = useStyles();

//   return (
//     <form className={classes.root} noValidate autoComplete="off">
//       <div>
//         <TextField
//           required
//           id="outlined-required"
//           label="Required"
//           defaultValue="Hello World"
//           variant="outlined"
//         />
//         <TextField
//           disabled
//           id="outlined-disabled"
//           label="Disabled"
//           defaultValue="Hello World"
//           variant="outlined"
//         />
//         <TextField
//           id="outlined-password-input"
//           label="Password"
//           type="password"
//           autoComplete="current-password"
//           variant="outlined"
//         />
//         <TextField
//           id="outlined-read-only-input"
//           label="Read Only"
//           defaultValue="Hello World"
//           InputProps={{
//             readOnly: true,
//           }}
//           variant="outlined"
//         />
//         <TextField
//           id="outlined-number"
//           label="Number"
//           type="number"
//           InputLabelProps={{
//             shrink: true,
//           }}
//           variant="outlined"
//         />
//         <TextField id="outlined-search" label="Search field" type="search" variant="outlined" />
// <TextField
//   id="outlined-helperText"
//   label="Helper text"
//   defaultValue="Default Value"
//   helperText="Some important text"
//   variant="outlined"
// />
//       </div>
//     </form>
//   );
// }
