const jwt = require("jsonwebtoken");
const User = require("../models/user");
const Board = require("../models/board");
require("../models/user");

exports.isAuth = (req, res, next) => {
  const auth = req.get("Authorization");
  // let board
  // console.log("isAuth.js", boardId);

  if (auth) {
    const token = auth.split(" ")[1];
    // console.log("token: ", token)

    const userData = jwt.verify(token, process.env.JWT_SECRET);

    // if()
    // console.log("check", !User.find(userData.userId) || Board.boardBelongsToUser(userData.userId, boardId))

    if (!User.find(userData.userId)) {
      console.log("smgs wrong with the token cheif");
      res.status(401).json({ message: "Unauthorized" });
      return;
    }
    // console.log("auth middleware");
    req.userId = userData.userId;
    next();
    return;
  }
  console.log("smgs wrong with the token cheif");
  res.status(401).json({ message: "Unauthorized" });
};

exports.userOwnsBoard = async (req, res, next) => {
  const auth = req.get("Authorization");
  const boardId = req.get("BoardId");

  // if (auth && boardId) {

  //   const userData = jwt.verify(token, process.env.JWT_SECRET);
  //   console.log(await Board.boardBelongsToUser(userData.userId, boardId))

  //   if (!await Board.boardBelongsToUser(userData.userId, boardId)) {
  //     res.status(401).json({ message: "Unauthorized" });
  //     return;
  //   }
  //   next();
  //   return;
  // }
  // res.status(401).json({ message: "Unauthorized" });
  if (!auth || !boardId) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  const token = auth.split(" ")[1];
  const userData = jwt.verify(token, process.env.JWT_SECRET);

  if (!(await Board.boardBelongsToUser(userData.userId, boardId))) {
    res.status(401).json({ message: "Unauthorized" });
    return;
  }

  next();
};
