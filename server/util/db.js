const mongodb = require("mongodb");
const MongoClient = mongodb.MongoClient;

let db;
const mongoConnect = (callback) => {
  MongoClient.connect(
    process.env.MONGODB_URI ||
      `mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@cluster0.i9jgi.gcp.mongodb.net/treclone?retryWrites=true&w=majority`,
    { useNewUrlParser: true, useUnifiedTopology: true }
  )
    .then((result) => {
      console.log("Connected to db");
      db = result.db();
      callback(result);
    })
    .catch((error) => {
      console.log(error);
    });
};

const getDb = () => {
  if (db) {
    return db;
  }
  throw "database is not working chief";
};

exports.mongoConnect = mongoConnect;
exports.getDb = getDb;

// mongodb+srv://schalkakos:YobrO4874@cluster0.i9jgi.gcp.mongodb.net/treclone?retryWrites=true&w=majority

// `mongodb://localhost:27017/${process.env.DB_NAME}?authSource=admin`
