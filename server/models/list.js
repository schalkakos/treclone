const mongodb = require("mongodb");

const { getDb } = require("../util/db");
const Card = require("../models/card");

let db;

class List {
  constructor(_id, title, order, boardId, sortBy) {
    this._id = _id;
    this.title = title;
    this.order = order;
    this.boardId = boardId;
    this.sortBy = sortBy ? sortBy : { order: 1 };
  }

  save() {
    db = getDb();
    return db
      .collection("lists")
      .insertOne({
        title: this.title,
        order: this.order,
        boardId: new mongodb.ObjectId(this.boardId),
        sortBy: this.sortBy,
        created: new Date(),
        lastModified: new Date(),
      })
      .then((result) => {
        result.ops[0].cards = [];
        return result.ops[0];
      })
      .catch((error) => {
        console.log(error);
      });
  }

  static updateById(listId, updatedData) {
    db = getDb();

    return db
      .collection("lists")
      .updateOne(
        { _id: new mongodb.ObjectId(listId) },
        {
          $set: updatedData,
        }
      )
      .then((result) => {
        return result;
      })
      .catch((error) => console.log(error));
  }

  static async updateListOrder(oldOrder, newOrder, listId, boardId) {
    db = getDb();

    let lowerValue = oldOrder < newOrder ? oldOrder : newOrder;
    let higherValue = oldOrder < newOrder ? newOrder : oldOrder;

    const lists = await db
      .collection("lists")
      .find({
        $and: [
          { boardId: new mongodb.ObjectId(boardId) },
          { _id: { $ne: new mongodb.ObjectId(listId) } },
          { order: { $gte: lowerValue } },
          { order: { $lte: higherValue } },
        ],
      })
      .project({ _id: 1, order: 1, title: 1 })
      .sort({ order: 1 })
      .toArray();

    let orderIterator = oldOrder < newOrder ? oldOrder : newOrder + 1;
    let bulkWriteTask = {};

    const buldWriteOperation = lists.map((obj, i) => {
      obj.order = orderIterator;
      orderIterator++;
      if (oldOrder < newOrder && orderIterator === newOrder) {
        orderIterator++;
      }
      // console.log(obj);
      bulkWriteTask = {
        updateOne: {
          filter: { _id: new mongodb.ObjectId(obj._id) },
          update: { $set: { order: obj.order } },
        },
      };
      return bulkWriteTask;
    });

    buldWriteOperation.push({
      updateOne: {
        filter: { _id: new mongodb.ObjectId(listId) },
        update: { $set: { order: newOrder } },
      },
    });

    return await db.collection("lists").bulkWrite(buldWriteOperation);
  }

  static findById(listId) {
    db = getDb();

    let list;
    return db
      .collection("lists")
      .findOne({ _id: new mongodb.ObjectId(listId) })
      .then((listObject) => {
        if (!listObject) {
          const error = new Error("List not found");
          throw error;
        }
        list = listObject;
        return Card.fetchAllByListId(listId, listObject.sortBy);
      })
      .then((cards) => {
        if (!cards) {
          const error = new Error(
            "Could not find the cards belonging to the list"
          );
          throw error;
        }
        list.cards = cards;

        return list;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  static fetchAllByBoardId(boardId) {
    db = getDb();
    return db
      .collection("lists")
      .find({ boardId: new mongodb.ObjectId(boardId) })
      .sort({ order: 1 })
      .toArray();
  }

  static deleteById(listId) {
    db = getDb();
    let listData = {};
    return db
      .collection("lists")
      .deleteOne({ _id: new mongodb.ObjectId(listId) })
      .then((deletedListResult) => {
        // console.log(deletedListResult)
        listData = deletedListResult;
        Card.deleteByListId(listId);
      })
      .then((deletedCardResult) => {
        return listData;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  static deleteByBoardId(boardId) {
    db = getDb();

    return db
      .collection("lists")
      .deleteMany({
        boardId: new mongodb.ObjectId(boardId),
      })
      .then((result) => {
        return result;
      });
  }
}

module.exports = List;
