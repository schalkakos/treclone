const mongodb = require("mongodb");
const { getDb } = require("../util/db");

const ChecklistItem = require("./checklistItem");

let db;

class Card {
  constructor(_id, listId, title, description, order) {
    this._id = _id;
    this.listId = listId;
    this.title = title;
    this.description = description;
    this.order = order;
  }

  save() {
    db = getDb();

    return db.collection("cards").insertOne({
      listId: new mongodb.ObjectId(this.listId),
      title: this.title,
      description: this.description,
      order: this.order,
      dueDate: null,
      dueDateDone: false,
      checklist: false,
      checklistName: "",
      checklistItems: [],
      created: new Date(),
      lastModified: new Date(),
    });
  }

  static updateById(cardId, updateData) {
    db = getDb();

    Object.keys(updateData).map((key) => {
      if (key.includes("id") || key.includes("Id")) {
        updateData[key] = new mongodb.ObjectId(updateData[key]);
      }
    });

    updateData.lastModified = new Date();

    return db
      .collection("cards")
      .updateOne(
        { _id: new mongodb.ObjectId(cardId) },
        {
          $set: updateData,
        }
      )
      .then((result) => {
        return result;
      });
  }

  static async updateCardOrderSameList(
    oldOrder,
    newOrder,
    cardId,
    listId,
    boardId
  ) {
    db = getDb();

    const lowerValue = oldOrder < newOrder ? oldOrder : newOrder;
    const higherValue = oldOrder < newOrder ? newOrder : oldOrder;

    const cards = await db
      .collection("cards")
      .find({
        $and: [
          { listId: new mongodb.ObjectId(listId) },
          { _id: { $ne: new mongodb.ObjectId(cardId) } },
          { order: { $gte: lowerValue } },
          { order: { $lte: higherValue } },
        ],
      })
      .project({ _id: 1, order: 1, title: 1 })
      .sort({ order: 1 })
      .toArray();

    // console.log("cards array", cards);

    let orderIterator = oldOrder < newOrder ? oldOrder : newOrder + 1;
    // let bulkWriteTask = {};

    let bulkWriteOperation = cards.map((obj) => {
      obj.order = orderIterator;
      orderIterator++;
      // if (oldOrder < newOrder && orderIterator === newOrder) {
      //   orderIterator++;
      // }
      // console.log(obj);
      let bulkWriteTask = {
        updateOne: {
          filter: { _id: new mongodb.ObjectId(obj._id) },
          update: { $set: { order: obj.order } },
        },
      };
      return bulkWriteTask;
    });

    bulkWriteOperation.push({
      updateOne: {
        filter: { _id: new mongodb.ObjectId(cardId) },
        update: { $set: { order: newOrder } },
      },
    });

    return await db.collection("cards").bulkWrite(bulkWriteOperation);
  }

  static async updateCardOrderDifferentList(
    oldOrder,
    newOrder,
    cardId,
    oldListId,
    newListId,
  ) {
    db = getDb();



    //get the cards from the old list that need to be updated in an array
    const oldListCards = await db
      .collection("cards")
      .find({
        $and: [
          { listId: new mongodb.ObjectId(oldListId) },
          { order: { $gt: oldOrder } },
        ],
      })
      .project({ _id: 1, order: 1, title: 1 })
      .sort({ order: 1 })
      .toArray();

    let listIterator = oldOrder;

    //replace the content of the array, with the bulkWrite operators using the fetched data
    const oldListBulkWriteOperation = oldListCards.map((obj) => {
      obj.order = listIterator;
      listIterator++;

      let bulkWriteTask = {
        updateOne: {
          filter: { _id: new mongodb.ObjectId(obj._id) },
          update: { $set: { order: obj.order } },
        },
      };

      return bulkWriteTask;
    });

    //get the cards from the new list that need to be updated in an array
    const newListCards = await db
      .collection("cards")
      .find({
        $and: [
          { listId: new mongodb.ObjectId(newListId) },
          { order: { $gte: newOrder } },
        ],
      })
      .project({ _id: 1, order: 1, title: 1 })
      .sort({ order: 1 })
      .toArray();

    listIterator = newOrder + 1;
    // console.log(listIterator);

    //same replacement as before just with cards from the new list
    const newListBulkWriteOperation = newListCards.map((obj) => {
      obj.order = listIterator;
      listIterator++;

      let bulkWriteTask = {
        updateOne: {
          filter: { _id: new mongodb.ObjectId(obj._id) },
          update: { $set: { order: obj.order } },
        },
      };

      return bulkWriteTask;
    });

    //concat the to arrays into one
    let bulkWriteOperation = oldListBulkWriteOperation.concat(
      newListBulkWriteOperation
    );

    //last oparation where we update the moved card to have the correct data
    bulkWriteOperation.push({
      updateOne: {
        filter: { _id: new mongodb.ObjectId(cardId) },
        update: {
          $set: { order: newOrder, listId: new mongodb.ObjectId(newListId) },
        },
      },
    });

    return await db.collection("cards").bulkWrite(bulkWriteOperation);
  }

  static findById(cardId) {
    db = getDb();
    let card;
    return db
      .collection("cards")
      .findOne({ _id: new mongodb.ObjectId(cardId) })
      .then((CardResult) => {
        card = CardResult;
        return ChecklistItem.fetchAllByCardId(cardId);
      })
      .then((checlistItems) => {
        card.checklistItems = checlistItems;
        return card;
      });
  }

  static fetchAllByListId(listId, sort = { order: -1 }) {
    db = getDb();

    return db
      .collection("cards")
      .find({ listId: new mongodb.ObjectId(listId) })
      .sort(sort)
      .toArray()
      .then((cardResult) => {
        return Promise.all(
          cardResult.map((obj) => {
            Promise.resolve(
              ChecklistItem.fetchAllByCardId(obj._id)
                .then((result) => {
                  return result;
                })
                .then((result) => {
                  return (obj.checklistItems = result);
                })
            );
            return obj;
          })
        );
      })
      .catch((error) => console.log(error));
  }

  static deleteById(cardId) {
    db = getDb();
    return db
      .collection("cards")
      .deleteOne({ _id: new mongodb.ObjectId(cardId) })
      .then((deletedChecklistResult) => {
        ChecklistItem.deleteByCardId(cardId);
      })
      .catch((error) => console.log(error));
  }

  static deleteByListId(listId) {
    db = getDb();
    return db
      .collection("cards")
      .deleteMany({ listId: new mongodb.ObjectId(listId) })
      .then((result) => {
        return result;
      })
      .catch((error) => console.log(error));
  }
}

module.exports = Card;
