const mongodb = require("mongodb");
const { getDb } = require("../util/db");

let db;

class ChecklistItem {
  constructor(cardId, name, _id = null, done = false) {
    this._id = _id;
    this.cardId = cardId;
    this.name = name;
    this.done = done;
  }

  save() {
    db = getDb();
    if (!this._id) {
      return db
        .collection("checklistItems")
        .insertOne({
          cardId: new mongodb.ObjectId(this.cardId),
          name: this.name,
          done: false,
          created: new Date(),
          lastModified: new Date(),
        })
        .then((result) => {
          return result;
        });
    } else {
      return db.collection("checklistItems").updateOne(
        { _id: new mongodb.ObjectId(this._id) },
        {
          $set: {
            name: this.name,
            done: this.done,
            lastModified: new Date(),
          },
        }
      );
    }
  }

  static updateById(_id, updateData) {
    db = getDb();

    Object.keys(updateData).map((key) => {
      if (key.includes("id") || key.includes("Id")) {
        updateData[key] = new mongodb.ObjectId(updateData[key]);
      }
    });

    updateData.lastModified = new Date();

    return db
      .collection("checklistItems")
      .updateOne({ _id: new mongodb.ObjectId(_id) }, { $set: updateData })
      .catch((error) => {
        console.log(error);
      });
  }

  static findById(checklistItemId) {
    db = getDb();
    return db
      .collection("checklistItems")
      .findOne({ _id: new mongodb.ObjectId(checklistItemId) });
  }

  static fetchAllByCardId(cardId) {
    db = getDb();
    return db
      .collection("checklistItems")
      .find({ cardId: new mongodb.ObjectId(cardId) })
      .sort({ created: 1 })
      .toArray()
      .catch((error) => {
        console.log(error);
      });
  }

  static deleteById(checklistItemId) {
    db = getDb();
    return db
      .collection("checklistItems")
      .deleteOne({ _id: new mongodb.ObjectId(checklistItemId) });
  }

  static deleteByCardId(cardId) {
    db = getDb();
    return db
      .collection("checklistItems")
      .deleteMany({ cardId: new mongodb.ObjectId(cardId) })
      .then((result) => {
        return result;
      })
      .catch((error) => console.log(error));
  }
}

module.exports = ChecklistItem;
