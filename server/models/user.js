const mongodb = require("mongodb");

const { getDb } = require("../util/db");

let db;

class User {
  constructor(email, password) {
    this.email = email;
    this.password = password;
  }

  save() {
    db = getDb();

    return db
      .collection("users")
      .insertOne({
        email: this.email,
        password: this.password,
        created: new Date(),
      })
      .then((result) => {
        return result.ops[0];
      })
      .catch((error) => {
        console.log(error);
      });
  }

  static async findById(userId) {
    db = getDb();
    let user;
    try {
      user = await db
        .collection("users")
        .findOne({ _id: new mongodb.ObjectId(userId) });
    } catch (error) {
      console.log(error);
    }
    return user;
  }

  static async findbyEmail(email) {
    db = getDb();

    return await db.collection("users").findOne({ email: email });
  }

  static async find(userId) {
    db = getDb();

    const user = await db.collection("users").findOne({_id: new mongodb.ObjectId(userId)})
    if(user) {
      return true;
    }
    return false;
  }
}

module.exports = User;
