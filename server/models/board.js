const mongodb = require("mongodb");

const { getDb } = require("../util/db");
const List = require("../models/list");
const Card = require("../models/card");

let db;

class Board {
  constructor(_id, name, ownerId) {
    this._id = _id;
    this.name = name;
    this.ownerId = ownerId;
  }

  save() {
    db = getDb();

    return db
      .collection("boards")
      .insertOne({
        name: this.name,
        ownerId: new mongodb.ObjectId(this.ownerId),
        created: new Date(),
        lastModified: new Date(),
      })
      .then((result) => {
        return result.ops[0];
      })
      .catch((error) => {
        console.log(error);
      });
    // } else {
    // db.collection("boards")
    //   .updateOne(
    //     {
    //       _id: new mongodb.ObjectId(this._id),
    //     },
    //     {
    //       $set: {
    //         name: this.name,
    //         ownerId: new mongodb.ObjectId(this.ownerId),
    //         lastModified: new Date(),
    //       },
    //     }
    //   )
    //   .then((result) => {
    //     return result;
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
    // }
  }

  // static updateById(name, ownerId) {
  //   db = getDb();

  //   db.collection("boards")
  //     .updateOne(
  //       {
  //         _id: new mongodb.ObjectId(this._id),
  //       },
  //       {
  //         $set: {
  //           name: this.name,
  //           ownerId: new mongodb.ObjectId(this.ownerId),
  //           lastModified: new Date(),
  //         },
  //       }
  //     )
  //     .then((result) => {
  //       return result;
  //     })
  //     .catch((error) => {
  //       console.log(error);
  //     });
  // }

  static findById(boardId) {
    db = getDb();
    let board;

    return db
      .collection("boards")
      .findOne({ _id: new mongodb.ObjectId(boardId) })
      .then((boardObject) => {
        if (!boardObject) {
          const error = new Error("Board was not found in the database");
          throw error;
        }
        board = boardObject;
        return List.fetchAllByBoardId(boardId);
      })
      .then(async (listsArray) => {
        if (!listsArray) {
          const error = new Error(
            "Something went wrong when trying to fetch board data"
          );
          throw error;
        }
        board.boardContent = listsArray;

        //   await Promise.all(board.boardContent.forEach(async (i) => {
        //   const cardsArray = await Card.fetchAllByListId(i._id);
        //   return i.cards = cardsArray;
        // }))

        for (const cv of board.boardContent) {
          const cardsArray = await Card.fetchAllByListId(cv._id, cv.sortBy);
          cv.cards = cardsArray;
        }

        return board;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  static fetchAllByUserId(userId) {
    db = getDb();

    return db
      .collection("boards")
      .find({ ownerId: new mongodb.ObjectId(userId) })
      .project({ name: 1 })
      .toArray()
      .then((result) => {
        return result;
      })
      .catch((error) => {
        console.log(error);
      });
  }

  static deleteBoard(boardId) {
    db = getDb();

    return db
      .collection("boards")
      .deleteOne({ _id: new mongodb.ObjectId(boardId) })
      .then((result) => {
        return db
          .collection("lists")
          .find({ boardId: new mongodb.ObjectId(boardId) })
          .forEach((doc) => {
            Card.deleteByListId(doc._id);
          })
          .then((result) => {
            return List.deleteByBoardId(boardId);
          })
          .then((result) => {
            return result;
          });
      })
      .catch((error) => console.log(error));
  }

  static async boardBelongsToUser(userId, boardId) {
    db = getDb();

    const boardData = await db.collection("boards").findOne({
      _id: new mongodb.ObjectId(boardId),
      ownerId: new mongodb.ObjectId(userId),
    });

    return boardData ? true : false;
  }
}

module.exports = Board;
