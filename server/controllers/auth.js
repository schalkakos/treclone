const User = require("../models/user");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
// const { hash } = require("bcrypt");
const Joi = require("joi");
// const { required } = require("joi");

exports.signup = async (req, res, next) => {
  const data = req.body;



  try {
    const emailCheck = await User.findbyEmail(data.email);
    if (emailCheck) {
      const error = new Error("Email already exists");
      error.statusCode = 401;
      error.field = "email";
      throw error;
    }

    const schema = Joi.object().keys({
      email: Joi.string().email().required().messages({
        "string.email": `Email must be a valid email`,
        "string.required": `Email is required`,
        //   "any.required": `"a" is a required field`,
      }),
      password: Joi.string().min(8).required().messages({
        "string.min": `password should have a minimum length of {#limit}`,
        "string.required": `password is required`,
      }),
      confirmPassword: Joi.any()
        .valid(Joi.ref("password"))
        .required()
        .messages({
          "any.required": "confirm password is required",
          "any.only": "the two passwords must match",
        }),
    });

    const result = schema.validate(data);

    if (result.error) {
      const error = new Error(result.error.details[0].message);
      error.statusCode = 401;
      error.field = result.error.details[0].path[0];
      throw error
    }
    
    const hashedPassword = await bcrypt.hash(data.password, 12);
    const user = new User(data.email, hashedPassword);

    const userInfo = await user.save();
    res
      .status(201)
      .json({ message: "user created successfully", userId: userInfo._id });
  } catch (error) {
    console.log(error);
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};

exports.login = async (req, res, next) => {
  const { email, password } = req.body;
  try {
    const user = await User.findbyEmail(email);

    if (!user) {
      const error = new Error("Email not found in the database");
      error.statusCode = 401;
      error.field = "email";
      throw error;
    }

    const isEqual = await bcrypt.compare(password, user.password);

    if (!isEqual) {
      const error = new Error("Wrong password");
      error.statusCode = 401;
      error.field = "password";
      throw error;
    }

    const expiresIn = "10800";
    const token = jwt.sign(
      {
        userId: user._id,
      },
      process.env.JWT_SECRET,
      { expiresIn: "3h" }
    );

    res.status(200).json({
      token: token,
      userId: user._id,
      expiresIn: expiresIn,
    });
  } catch (error) {
    console.log(error);
    if (!error.statusCode) {
      error.statusCode = 500;
    }
    next(error);
  }
};
