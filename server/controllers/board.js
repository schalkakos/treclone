const Board = require("../models/board");
const jwt = require("jsonwebtoken");

exports.createBoard = async (req, res, next) => {
  const name = req.body.name;
  const ownerId = req.body.ownerId;

  const board = new Board(null, name, ownerId);

  const createdBoard = await board.save()

  res.status(201).json({ message: "Board created successfully", createdBoard: createdBoard });
};

exports.updateBoard = (req, res, next) => {
  const _id = req.body._id;
  const name = req.body.name;
  const ownerId = req.body.ownerId;

  const board = new Board(_id, name, ownerId);
  board.save();
  res.status(200).json({ message: "Board updated successfully" });
};

exports.getBoard = (req, res, next) => {
  const boardId = req.params.boardId;
  // console.log("boardId", boardId)
  Board.findById(boardId).then((result) => {
    res.json({ result });
  });
};

exports.getBoards = async (req, res, next) => {
  const { userId } = req.params;

  const auth = req.get("Authorization");

  const token = auth.split(" ")[1];
  // console.log("token: ", token)

  const userData = jwt.verify(token, process.env.JWT_SECRET);

  if(userId !== userData.userId) {
    res.status(401).json({ message: "Unauthorized" })
    return;
  }

  const boards = await Board.fetchAllByUserId(userId);

  res.status(200).json({boards: boards});
};


exports.deleteBoard = (req, res, next) => {
  const boardId = req.body.boardId;
  Board.deleteBoard(boardId).then((result) => {
    res
      .status(200)
      .json({
        message: "The board and everything belonging to it was deleted ",
      });
  });
};
