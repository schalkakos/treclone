const List = require("../models/list");

exports.getList = (req, res, next) => {
  const listId = req.params.listId;

  List.findById(listId).then((result) => {
    res.status(200).json({
      result: result,
    });
  });
};

exports.createList = (req, res, next) => {
  const title = req.body.title;
  const order = req.body.order;
  const boardId = req.body.boardId;
  const list = new List(null, title, order, boardId);

  list.save().then((result) => {
    res
      .status(201)
      .json({ result: result, message: "List created succesfully" });
  });
};

exports.updateList = (req, res, next) => {
  const _id = req.body._id;
  const reqBody = req.body;
  delete reqBody._id;

  List.updateById(_id, reqBody).then((result) => {

    res.status(200).json({ message: "List updated succesfully" });
  });
};

exports.deleteList = (req, res, next) => {
  const listId = req.body.listId;

  List.deleteById(listId).then((result) => {
    if (result.deletedCount) {
      res.status(200).json({ message: "deleted everything" });
    } else if (result.deletedCount === 0) {
      const error = new Error("list not found");
      error.statusCode = 404;
      next(error);
    }
  });
};

exports.updateListOrder = (req, res, next) => {
  console.time("request");
  const { listId, boardId, oldOrder, newOrder } = req.body;

  List.updateListOrder(oldOrder, newOrder, listId, boardId).then((result) => {
    // console.log(result);
    if (result.matchedCount === result.modifiedCount) {
      console.timeEnd(`request`);
      res.status(200).json({ message: "list order has been updated" });
    } else {
      const error = new Error("something went wrong with the list order udpate");
      error.statusCode = 404;
      next(error);
      // res
      //   .status(404)
      //   .json({ message: "something went wrong with the list order udpate" });
    }
  });
};
