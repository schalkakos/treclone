const ChecklistItem = require("../models/checklistItem");

exports.createChecklistItem = (req, res, next) => {
  const cardId = req.body.cardId;
  const name = req.body.name;

  checklistItem = new ChecklistItem(cardId, name);
  checklistItem.save(cardId, name).then((result) => {
    const insertedData = result.ops[0];

    res.json({
      message: "checklist item created",
      response: insertedData,
    });
  });
};

exports.updateChecklistItem = (req, res, next) => {
  // const _id = req.body._id
  // const cardId = req.body.cardId;
  // const name = req.body.name;
  // const done = req.body.done

  const reqBody = req.body;
  const _id = req.body._id;
  delete reqBody._id;

  ChecklistItem.updateById(_id, reqBody).then((result) => {
    res.json({
      response: "checklist item udpated",
    });
  });
};

exports.getChecklistItem = (req, res, next) => {
  const checklistItemId = req.params.checklistItemId;

  ChecklistItem.findById(checklistItemId).then((result) => {
    res.json({
      response: result,
    });
  });
};

exports.deleteChecklistItem = (req, res, next) => {
  const checklistItem = req.params.checklistId;
  
  ChecklistItem.deleteById(checklistItem).then((result) => {
    res.json({
      response: "checklist item deleted",
    });
  });
};

exports.deleteChecklistItemsByCardId = (req, res, next) => {
  ChecklistItem.deleteByCardId(req.body.cardId).then((result) => {
    res.json({
      message: "checklist items belonging to the given cardId were deleted",
    });
  });
};
