const Card = require("../models/card");

exports.getCard = (req, res, next) => {
  const cardId = req.params.cardId;
  Card.findById(cardId)
    .then((result) => {
      // console.log(result)
      if (!result) {
        // console.log("status:404. Card not found in database");
        res.status(404).json({ messeage: "Card not found in database" });
      }
      res.status(200).json({
        response: result,
      });
    })
    .catch((error) => {
      console.log(error);
    });
};

exports.updateCard = (req, res, next) => {
  const reqBody = req.body;
  const _id = reqBody._id;
  delete reqBody._id;

  Card.updateById(_id, reqBody).then((result) => {
    if (result.modifiedCount === 0) {
      const error = new Error("Card not found in database");
      error.statusCode = 404;
      next(error);
    } else {
      res.status(200).json({ message: "Card updated successfully" });
    }
  });
};

exports.createCard = (req, res, next) => {
  const listId = req.body.listId;
  const title = req.body.title;
  const description = req.body.description;
  const order = req.body.order;
  const card = new Card(null, listId, title, description, order);
  card.save().then((result) => {
    res.status(201).json({
      response: result.ops[0],
      message: "Card successfully created",
    });
  });
};

exports.deleteCard = (req, res, next) => {
  const cardId = req.body.cardId;

  Card.deleteById(cardId).then((result) => {
    res.status(200).json({
      message: "Card deleted successfully",
    });
  });
};

exports.updateCardOrder = async (req, res, next) => {
  const { oldOrder, newOrder, cardId } = req.body;

  let result = {};
  if (req.body.oldListId && req.body.newListId) {
    const { oldListId, newListId } = req.body;

    result = await Card.updateCardOrderDifferentList(
      oldOrder,
      newOrder,
      cardId,
      oldListId,
      newListId
    );
  } else {
    const { listId } = req.body;

    result = await Card.updateCardOrderSameList(
      oldOrder,
      newOrder,
      cardId,
      listId
    );
  }

  res.status(200).json({
    message: "done moveing cards around check the result",
  });
};
