const express = require("express");

const listController = require("../controllers/list");
const isAuth = require("../middlewares/isAuth").isAuth;
const userOwnsBoard = require("../middlewares/isAuth").userOwnsBoard;

const router = express.Router();

router.get("/:listId", isAuth, userOwnsBoard, listController.getList);
router.post("/", isAuth, userOwnsBoard, listController.createList);
router.patch("/order/", isAuth, userOwnsBoard, listController.updateListOrder);
router.patch("/", isAuth, userOwnsBoard, listController.updateList);
router.delete("/", isAuth, userOwnsBoard, listController.deleteList);

module.exports = router;