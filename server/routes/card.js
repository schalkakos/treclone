const express = require('express');

const cardController = require('../controllers/card');
const isAuth = require("../middlewares/isAuth").isAuth;
const userOwnsBoard = require("../middlewares/isAuth").userOwnsBoard;

const router = express.Router();

router.get('/:cardId', isAuth, userOwnsBoard, cardController.getCard);
router.post('/', isAuth, userOwnsBoard, cardController.createCard);
router.patch('/', isAuth, userOwnsBoard, cardController.updateCard);
router.patch('/order/', isAuth, userOwnsBoard, cardController.updateCardOrder);
router.delete('/', isAuth, userOwnsBoard, cardController.deleteCard);

module.exports = router;