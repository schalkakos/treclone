const express = require("express");

const boardController = require("../controllers/board");
const isAuth = require("../middlewares/isAuth").isAuth;
const userOwnsBoard = require("../middlewares/isAuth").userOwnsBoard;

const router = express.Router();

router.post("/board", isAuth, boardController.createBoard);
router.patch("/board", isAuth,userOwnsBoard, boardController.updateBoard);
router.get("/board/:boardId", isAuth, userOwnsBoard, boardController.getBoard);
// router.get("/user/:userId", boardController.getBoards);
router.delete("/board/:boardId", isAuth, userOwnsBoard, boardController.deleteBoard);
router.get("/:userId/boards", isAuth, boardController.getBoards)

module.exports = router;