const express = require("express");

const checklistItemController = require("../controllers/checklistItem");
const isAuth = require("../middlewares/isAuth").isAuth;
const userOwnsBoard = require("../middlewares/isAuth").userOwnsBoard;

const router = express.Router();

router.get("/:checklistItemId", isAuth, userOwnsBoard, checklistItemController.getChecklistItem);
router.patch("/", isAuth, userOwnsBoard, checklistItemController.updateChecklistItem);
router.post("/", isAuth, userOwnsBoard, checklistItemController.createChecklistItem);
router.delete("/", isAuth, userOwnsBoard, checklistItemController.deleteChecklistItemsByCardId);
router.delete("/:checklistId", isAuth, userOwnsBoard, checklistItemController.deleteChecklistItem);

module.exports = router;
