const express = require("express");
const bodyParser = require("body-parser");
const helmet = require("helmet");
const multer = require("multer");
const cors = require("cors");
const cookieParser = require('cookie-parser'); 

const {mongoConnect} = require("./util/db");
const authRouter = require('./routes/auth');
const cardRouter = require('./routes/card');
const listRouter = require('./routes/list');
const checklistItem = require('./routes/checklistItem');
const boardRouter = require('./routes/board');



const app = express();
require('dotenv').config();
app.use(helmet());
// app.use(cors({ origin: 'sad-borg-c50593.netlify.app', credentials: true, exposedHeaders: ["set-cookie"] }));
app.use(cors());
app.use(cookieParser());

app.use(bodyParser.json());

app.use("/", authRouter);
app.use("/card", cardRouter);
app.use("/list", listRouter);
app.use("/checklist", checklistItem);
app.use(boardRouter);
app.use((error, req, res, next) => {
  console.log(error);
  if(error.statusCode === 401) {
    res.status(error.statusCode).json({message: error.message, field: error.field});
    return;
  }
  res.status(error.statusCode).json({message: "Something smells fishy"});
});

const port = process.env.PORT || 8080;
mongoConnect(() => {
  app.listen(port);
})
