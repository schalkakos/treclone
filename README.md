﻿# Easy-tasks
A Task manager webapp heavily based on Trello. Currently still in development.

## General info
This was my second big project and the first time i used React, Node.js, Express and MongoDB. I decided to make a bigger project because it constantly makes me face challenges and helps me get a better understanding of how things work and there limitations, that i couldn't get with small projects.

## Built With
- Frontend
    * React / React Router / React Saga 
    * Redux
- Backend
    * Node.js
    * Express.js
    * MongoDB

## To-do list:
* Add Authentication
* Add functionalities to the board, the lists, and the cards like:
    * Create/Delete boards
    * Add deadlines to tasks
    * Make lists/cards moveable between boards
    * Drag and Drop on lists/cards
    * Add tasks to cards with checkboxes and a progressbar of the completed tasks

## Status
Project is: _in development_

## Inspiration
The project is based on Trello